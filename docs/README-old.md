# DEMO-GROCERY #

## Development Workflow ##
This project use [Corebiz Workflow for VTEX IO](https://spaces.corebiz.ag/doc/vtex-io-PQOtxq8T7t).     
Read documentation and complies with all the requirements that our DevOps team establishes.

## Requirements ##  
Setup your development environment according to the operating system you use:
* [Linux Debian/Ubuntu](https://spaces.corebiz.ag/doc/linux-ubuntu-DHg18VopRi)
* [Mac OSx](https://spaces.corebiz.ag/doc/macos-Ua4gRLNbA1)
* [Windows](https://spaces.corebiz.ag/doc/windows-rlL0IDabL9)

**IMPORTANT**: Any other setup that does not include these tools can cause errors or problems during development. On Windows Corebiz CLI is only compatible with Ubuntu 20.04 LTS (WSL 2) terminals download from Microsoft store.

## Continuous Integration ##
This project have Continuous Integration and Continuous Delivery practices.    
For more information read [Continuous Integration](https://spaces.corebiz.ag/doc/continuous-integration-delivery-Tvbt9zjM0s) and [CI/CD stages](https://spaces.corebiz.ag/doc/cicd-stages-z07T957Yhs) articles.

## Environment ##
To see environments and active VTEX workspaces navigate to `Deployments > Environments`.    
For more information read [Environment docs](https://spaces.corebiz.ag/doc/cicd-stages-z07T957Yhs).

## Releases ##
[VTEX Releases](https://spaces.corebiz.ag/doc/vtex-releases-b59xkB1Xhw) are 100% automated by CI/CD process.   
To see all releases published navigate to `Deployments > Releases`.     

## Git Practices ##
Be sury to respect commit, branch and Merge request policies.    
For more information read [Git Practicies](https://spaces.corebiz.ag/doc/git-practices-SN3deVSj8G).

## Code Style / Quality ##
This project have code style and quality policies that must by respected.    
For more information read [Code Style / Quality](https://spaces.corebiz.ag/doc/code-quality-hgzdy05phZ).

## Project Documentation ##
Explore the project documentation.   
Here you cand find important information.
* [How use Store/App](/docs)
* [Changelog](/CHANGELOG.md)

## Troubleshooting ##
¿Are you having problems? visit [troubleshooting](https://spaces.corebiz.ag/doc/troubleshooting-EZFtypAnE7) section or request support in our slack channel [#devops-support](https://app.slack.com/client/T02FL153XD1/C0360C04F7U)
