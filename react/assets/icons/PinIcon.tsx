/* eslint-disable semi */
import React, { FunctionComponent } from 'react'

const PinIcon: FunctionComponent = () => (
  <svg
    width="24"
    height="24"
    viewBox="0 0 24 24"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <path
      d="M12.5 14.5C14.1569 14.5 15.5 13.1569 15.5 11.5C15.5 9.84315 14.1569 8.5 12.5 8.5C10.8431 8.5 9.5 9.84315 9.5 11.5C9.5 13.1569 10.8431 14.5 12.5 14.5Z"
      stroke="#D6647A"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      d="M18.157 17.157L13.914 21.4C13.539 21.7746 13.0306 21.9851 12.5005 21.9851C11.9704 21.9851 11.462 21.7746 11.087 21.4L6.843 17.157C5.72422 16.0382 4.96234 14.6127 4.65369 13.0609C4.34504 11.509 4.50349 9.90051 5.10901 8.43873C5.71452 6.97694 6.7399 5.72754 8.05548 4.8485C9.37107 3.96947 10.9178 3.50029 12.5 3.50029C14.0822 3.50029 15.6289 3.96947 16.9445 4.8485C18.2601 5.72754 19.2855 6.97694 19.891 8.43873C20.4965 9.90051 20.655 11.509 20.3463 13.0609C20.0377 14.6127 19.2758 16.0382 18.157 17.157V17.157Z"
      stroke="#D6647A"
      strokeWidth="1.5"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
)

export default PinIcon
