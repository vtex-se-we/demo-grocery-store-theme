/* eslint-disable semi */
import React, { FunctionComponent } from 'react'

interface IconRadioProps {
  color: string
}

const IconRadio: FunctionComponent<IconRadioProps> = ({ color }) => (
  <svg
    width="21"
    height="20"
    viewBox="0 0 21 20"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <circle
      cx="10.5"
      cy="10"
      r="7.25"
      fill="white"
      stroke="#DADEDC"
      strokeWidth="1.5"
    />
    <circle cx="10.5" cy="10" r="4" fill={color} />
  </svg>
)

export default IconRadio
