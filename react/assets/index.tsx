import Bazar from './icons/Bazar'
import Bebida from './icons/Bebida'
import CarnesAvesPescados from './icons/CarnesAvesPescados'
import Congelados from './icons/Congelados'
import Embalagens from './icons/Embalagens'
import FriosLaticineos from './icons/FriosLaticineos'
import Higiene from './icons/Higiene'
import Hortifruti from './icons/Hortifruti'
import Limpeza from './icons/Limpeza'
import Mercearia from './icons/Mercearia'
import Padaria from './icons/Padaria'
import Petshop from './icons/Petshop'
import Todos from './icons/Todos'
import SelectedBazar from './icons/SelectedBazar'
import SelectedBebida from './icons/SelectedBebida'
import SelectedCarnesAvesPescados from './icons/SelectedCarnesAvesPescados'
import SelectedCongelados from './icons/SelectedCongelados'
import SelectedEmbalagens from './icons/SelectedEmbalagens'
import SelectedFriosLaticineos from './icons/SelectedFriosLaticineos'
import SelectedHigiene from './icons/SelectedHigiene'
import SelectedHortifruti from './icons/SelectedHortifruti'
import SelectedLimpeza from './icons/SelectedLimpeza'
import SelectedMercearia from './icons/SelectedMercearia'
import SelectedPadaria from './icons/SelectedPadaria'
import SelectedPetshop from './icons/SelectedPetshop'
import SelectedTodos from './icons/SelectedTodos'
import Lixeira from './icons/Lixeira'
import IconRadio from './icons/IconRadio'
import IconDelivery from './icons/IconDelivery'
import IconPickup from './icons/IconPickup'
import PinIcon from './icons/PinIcon'

export {
  Bazar,
  Bebida,
  CarnesAvesPescados,
  Congelados,
  Embalagens,
  FriosLaticineos,
  Higiene,
  Hortifruti,
  Limpeza,
  Mercearia,
  Padaria,
  Petshop,
  Todos,
  SelectedBazar,
  SelectedBebida,
  SelectedCarnesAvesPescados,
  SelectedCongelados,
  SelectedEmbalagens,
  SelectedFriosLaticineos,
  SelectedHigiene,
  SelectedHortifruti,
  SelectedLimpeza,
  SelectedMercearia,
  SelectedPadaria,
  SelectedPetshop,
  SelectedTodos,
  Lixeira,
  IconRadio,
  IconDelivery,
  IconPickup,
  PinIcon,
}
