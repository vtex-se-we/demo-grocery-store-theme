declare global {
  type CustomMenu = MenuLinks & {
    showSubmenu: boolean
    submenuItems: [MenuLinks]
  }

  type MenuLinks = {
    __editorItemTitle: string
    url: string
  }

  type PostalCodeInfo = {
    postalCode: string
    state: string
    country: string
  }
}

export {}
