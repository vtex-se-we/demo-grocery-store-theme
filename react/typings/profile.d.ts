interface Payment {
  id: string
  paymentSystem: string
  paymentSystemName: string
  cardNumber: string
}
