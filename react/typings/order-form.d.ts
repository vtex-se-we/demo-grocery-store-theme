import { ApolloError } from 'apollo-client'
import { OrderForm } from 'vtex.checkout-graphql'

type OrderFormUpdate =
  | Partial<OrderForm>
  | ((prevOrderForm: OrderForm) => Partial<OrderForm>)

interface OrderFormContext {
  loading: boolean
  orderForm: OrderForm
  setOrderForm: (nextValue: OrderFormUpdate) => void
  error: ApolloError | undefined
}

interface OrderShippingResponse {
  success: boolean
  orderForm?: OrderForm
}

interface OrderForm {
  id: string
  items: Item[]
  value: number
  totalizers: Totalizer[]
  marketingData: MarketingData
  canEditData: boolean
  loggedIn: boolean
  paymentData: PaymentData
  messages: Messages
  shipping: Shipping
  userProfileId: string
  userType: string
  clientProfileData: ClientProfileData
  clientPreferencesData: ClientPreferencesData
  allowManualPrice: boolean
}

interface ClientPreferencesData {
  locale: string
  optInNewsletter?: boolean
}

interface ClientProfileData {
  email: string
  firstName?: string
  lastName?: string
  document?: string
  documentType?: string
  phone?: string
  isValid: boolean
}

interface Shipping {
  countries: string[]
  availableAddresses: AvailableAddress[]
  selectedAddress: SelectedAddress
  deliveryOptions: DeliveryOption[]
  isValid: boolean
}

interface DeliveryOption {
  id: string
  price: number
  estimate: string
  isSelected: boolean
}

interface SelectedAddress {
  addressId: string
  addressType: string
  city: string
  complement: string
  country: string
  neighborhood: string
  number: string
  postalCode: string
  receiverName?: any
  reference?: any
  state: string
  street: string
  isDisposable: boolean
}

interface AvailableAddress {
  addressId: string
  addressType: string
  city?: string
  complement?: string
  country: string
  neighborhood?: string
  number: string
  postalCode: string
  receiverName?: string
  reference?: string
  state?: string
  street?: string
  isDisposable: boolean
}

interface Messages {
  couponMessages: any[]
  generalMessages: any[]
}

interface PaymentData {
  paymentSystems: PaymentSystem[]
  payments: Payment[]
  installmentOptions: InstallmentOption[]
  availableAccounts: any[]
  isValid: boolean
}

interface InstallmentOption {
  paymentSystem: string
  installments: Installment[]
}

interface Installment {
  count: number
  hasInterestRate: boolean
  interestRate: number
  value: number
  total: number
}

interface Payment {
  paymentSystem: string
  bin?: string
  accountId?: string
  tokenId?: string
  installments: number
  referenceValue: number
  value: number
}

interface PaymentSystem {
  id: string
  name: string
  groupName: string
  validator: Validator
  stringId: string
  requiresDocument: boolean
  isCustom: boolean
  description?: string
  requiresAuthentication: boolean
  dueDate: string
}

interface Validator {
  regex?: string
  mask?: string
  cardCodeRegex?: string
  cardCodeMask?: string
  weights?: number[]
  useCvv: boolean
  useExpirationDate: boolean
  useCardHolderName: boolean
  useBillingAddress: boolean
}

interface MarketingData {
  coupon: string
  utmCampaign: string
  utmMedium: string
  utmSource: string
  utmiCampaign: string
  utmiPart: string
  utmiPage: string
}

interface Totalizer {
  id: string
  name: string
  value: number
}

interface Item {
  additionalInfo: AdditionalInfo
  attachments: any[]
  attachmentOfferings: AttachmentOffering[]
  bundleItems: any[]
  parentAssemblyBinding?: any
  sellingPriceWithAssemblies?: any
  options?: any
  availability: string
  detailUrl: string
  id: string
  imageUrls: ImageUrls
  listPrice: number
  manualPrice?: any
  measurementUnit: string
  name: string
  offerings: any[]
  price: number
  productCategories: ProductCategories
  productCategoryIds: string
  productRefId: string
  productId: string
  quantity: number
  seller: string
  sellingPrice: number
  skuName: string
  skuSpecifications: any[]
  unitMultiplier: number
  uniqueId: string
  refId: string
  priceTags: any[]
}

type ProductCategories = Record<string, string>

interface ImageUrls {
  at1x: string
  at2x: string
  at3x: string
}

interface AttachmentOffering {
  name: string
  required: boolean
  schema: AttachmentSchema
}

type AttachmentSchema = Record<string, SchemaField>

interface SchemaField {
  maximumNumberOfCharacters: number
  domain?: any[]
}

interface AdditionalInfo {
  brandName: string
}
