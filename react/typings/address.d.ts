interface AddressWithValidation {
  addressId: ValidationField
  addressType: ValidationField
  city: ValidationField
  complement: ValidationField
  country: ValidationField
  geoCoordinates: GeoCoordinatesValidationField
  neighborhood: ValidationField
  number: ValidationField
  postalCode: ValidationField
  receiverName: ValidationField
  reference: ValidationField
  state: ValidationField
  street: ValidationField
  addressQuery: ValidationField
}

interface ValidationField {
  geolocationAutoCompleted?: boolean
  value?: string
  postalCodeAutoCompleted?: boolean
  geolocationAutoCompleted?: boolean
  valid?: boolean
  visited?: boolean
  disabled?: boolean
  loading?: boolean
  reason?: string
}

interface GeoCoordinatesValidationField {
  value?: number[]
  postalCodeAutoCompleted?: boolean
  geolocationAutoCompleted?: boolean
  valid?: boolean
  visited?: boolean
  disabled?: boolean
  loading?: boolean
  reason?: string
}

interface Address {
  addressId: string
  addressType: string
  city: string
  complement: string
  country: string
  geoCoordinates: number[]
  neighborhood: string
  number: string
  postalCode: string
  receiverName?: string
  reference?: string
  state: string
  street: string
  addressQuery?: string
  selectedSla?: string
}
