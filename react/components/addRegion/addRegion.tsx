/* eslint-disable-next-line jsx-a11y/no-static-element-interactions */
import { IconChevronDown } from '@tabler/icons'
import React, { useContext, useEffect, useState } from 'react'
import ContentLoader from 'react-content-loader'

import { RegionContext } from '../../context/Location'
import ShippingModal from '../shipping-modal/ShippingModal'
import styles from './addRegion.css'

const AddRegion: React.FC = () => {
  const { region, pickup } = useContext(RegionContext)
  const [isOpen, setOpen] = useState(false)
  const openModal = () => setOpen(true)

  useEffect(() => {
    window.addEventListener('mambo:openShippingModal', openModal)

    return () => {
      window.removeEventListener('mambo:openShippingModal', openModal)
    }
  }, [openModal])

  return (
    <div
      className={styles.containerRegion}
      aria-hidden
      onClick={() => setOpen(true)}
    >
      <div className={styles.containerInfo} title={region || pickup}>
        {!region && !pickup ? (
          <ContentLoader
            speed={1}
            width={88}
            height={29}
            viewBox="0 0 120 29"
            backgroundColor="#f3f3f3"
            foregroundColor="#ecebeb"
          >
            <rect x="0" y="7" rx="3" ry="3" width="52" height="6" />
            <rect x="0" y="22" rx="3" ry="3" width="88" height="6" />
          </ContentLoader>
        ) : (
          <>
            {region && (
              <span className={`${styles.containerSendFor} w-100`}>
                Enviar para:
              </span>
            )}
            {pickup && (
              <span className={`${styles.containerSendFor} w-100`}>
                Retirar em:
              </span>
            )}
            <span className={styles.containerLocation}>{region || pickup}</span>
            <IconChevronDown size={16} color="#000" viewBox="0 0 24 24" />
          </>
        )}
      </div>
      <ShippingModal isOpenModal={isOpen} onClose={() => setOpen(false)} />
    </div>
  )
}

export default AddRegion
