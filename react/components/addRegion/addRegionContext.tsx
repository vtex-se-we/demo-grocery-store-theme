import React from 'react'

import { RegionProvider } from '../../context/Location'
import AddRegion from './addRegion'

const AddRegionContext: React.FC = () => (
  <RegionProvider>
    <AddRegion />
  </RegionProvider>
)

export default AddRegionContext
