import React, { FC } from 'react'

import {
  ModalOverlay,
  ModalWrapper,
  ModalContent,
  MapWrapper,
  ConfirmationWrapper,
} from './styles'

type ModalProps = {
  isOpen: boolean
  onClose: () => void
}

export const Modal: FC<ModalProps> = ({ children, isOpen, onClose }) => {
  return (
    <>
      {isOpen && (
        <>
          <ModalOverlay role="presentation" onClick={onClose} />
          <ModalWrapper>
            {/* <ModalHeader>
                <ModalCloseButton onClick={onClose}>Fechar</ModalCloseButton>
              </ModalHeader> */}
            <MapWrapper id="map-wrapper-regionalization" />
            <ModalContent>
              {children}
              <ConfirmationWrapper id="confirmation-wrapper-regionalization" />
            </ModalContent>
          </ModalWrapper>
        </>
      )}
    </>
  )
}
