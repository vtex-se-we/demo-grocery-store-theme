import styled from 'styled-components'

export const ModalOverlay = styled.div`
  position: fixed;
  inset: 0;
  background-color: rgba(0, 0, 0, 0.3);
  z-index: 1010;
`
export const ModalWrapper = styled.div`
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  display: flex;
  z-index: 1011;
  width: 100%;
  pointer-events: none;

  > * {
    pointer-events: auto;
  }
`

export const ModalCloseButton = styled.button``

export const ModalContent = styled.div`
  width: 375px;
  background-color: #f7f7f7;
  margin-left: auto;
  position: relative;
`
export const ModalHeader = styled.header``

export const MapWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;

  &:empty {
    display: none;
  }

  @media screen and (max-width: 460px) {
    display: none;
  }
`

export const ConfirmationWrapper = styled.div`
  &:empty {
    display: none;
  }
`
