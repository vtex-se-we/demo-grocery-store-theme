/* eslint-disable */
import React, { useCallback,  useState } from 'react'
import { useUpdateSession } from 'vtex.session-client'
import Pickup from './Pickup'
import { usePickup } from './hook/usePickup'
import { Header } from './Header'
import  {SelectZipCode }  from './views/SelectZipCode'
import { PlaceProps } from './types'
import { Map, MarkerProps } from './views/Map'
import { Confirmation } from './views/Confirmation'
import {useRuntime} from 'vtex.render-runtime'
import { useIntl } from 'react-intl'
import { confirm } from '../../utils/definedMessages'

import { useOrderForm } from 'vtex.order-manager/OrderForm'
import axios from 'axios'

type NavigationState = 'Start' | 'ZipCode' | 'PickUp'

const SecondTab = () => {
  const updateSession = useUpdateSession()
  const [state, setState] = useState<NavigationState>('ZipCode')
  const [selectedPlace, setSelectedPlace] = useState<PlaceProps>(
    {} as PlaceProps
  )
  
  const intl = useIntl()
  const orderForm = useOrderForm()
  const { culture } = useRuntime()

  const [markersList, setMarkersList] = useState<MarkerProps[]>([])

  const { closeModal } = usePickup()
  const [dataToSend, setDataToSend] = useState<{
    SellerId: string
    Name: string
    geoCoordinates: any
  } | null>(null)

  async function onClick(SellerId: string, Name: string, geoCoordinates) {
    const response = await fetch(
      `/api/checkout/pub/regions?country=${culture.country}&geoCoordinates=${geoCoordinates}`
    )
    const data = await response.json()

    console.log('region id data: ', data[0].id)

    await fetch('/api/sessions', {
      method: 'POST',
      body: JSON.stringify({
        public: {
          regionId: {
            value: data?.[0].id,
          },
        },
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    })

    updateSession({
      variables: {
        fields: {
          Name,
          SellerId,
          postalCode: null,
          state: null,
          country: null,
        },
      },
      public: {
        regionId: {
          value: data?.[0].id,
        },
      },
    })
    closeModal()
  }

  const handleSelect = (place: PlaceProps) => {
    console.log('place: ', place)
    console.log('lat: ', place.geometry.location.lat())
    console.log('lng: ', place.geometry.location.lng())
    setState('PickUp')
    setSelectedPlace(place)
  }

  const handlePickupPoints = (markers: MarkerProps[]) => {
    setMarkersList(markers)
  }

  const handlePickUpSelect = (
    SellerId: string,
    Name: string,
    geoCoordinates: any
  ) => {
    setDataToSend({
      geoCoordinates,
      Name,
      SellerId,
    })
   
  }

  
  // const [simulateShipping] = useLazyQuery(
  //   SIMULATE_SHIPPING,
  //   {
  //     fetchPolicy: 'cache-and-network',
  //   }
  // )
 

  const handleEdit = () => {
    setDataToSend(null)
  }

  const handleSubmit = useCallback(() => {
    onClick(
      dataToSend?.SellerId ?? '',
      dataToSend?.Name ?? '',
      dataToSend?.geoCoordinates
    )
    RegionOF(selectedPlace,dataToSend?.geoCoordinates)
  }, [dataToSend])

  const handleNewSearch = () => {
    setState('ZipCode')
  }
  
  const RegionOF = (e,i) => {

    const latlong = i.split(";")

    const data = {
      items: [
        {
          id: "3",
          quantity: 1,
          seller: '1'
        }
      ],
      "geoCoordinates": [
        latlong[0],
        latlong[1]
      ],
      country: culture.country
    }

    axios.post('/api/checkout/pub/orderforms/simulation', data).then(function (response) {
      
      const body = {
        selectedAddresses: [
          {
            addressType: 'Pickup',
            geoCoordinates: [
              e.geometry.location.lng(),
              e.geometry.location.lat(),
            ],
            country: culture.country,
            street: e.address_components.filter((p) =>
              p.types.includes('route')
            )[0]?.long_name,
            city: e.address_components.filter((p) =>
              p.types.includes('locality')
            )[0]?.long_name,
            state: e.address_components.filter((p) =>
              p.types.includes('administrative_area_level_1')
            )[0]?.long_name,
          },
        ],
        logisticsInfo: [
          {
              itemIndex: 0,
              selectedDeliveryChannel: response.data.logisticsInfo[0].slas[0].deliveryChannel,
              selectedSla: response.data.logisticsInfo[0].slas[0].id
          }
        ]
      }
  
      if (orderForm) {
        axios
          .post(
            `/api/checkout/pub/orderForm/${orderForm?.orderForm?.id}/attachments/shippingData`,
            body
          )
          .then(() => {})
          .catch((err) => {
            console.log('error cartRegions', err)
          })
      }

    }).catch(function (error) {
      console.error(error);
    });

    
  }

  console.log(updateSession, 'teste update pickup')

  return (
    <div className="SecondTab">
      <Header />

      {state === 'ZipCode' && <SelectZipCode handleSelect={handleSelect} />}
      {state === 'PickUp' && (
        <Pickup
          onClick={handlePickUpSelect}
          markers={markersList}
          handleNewSearch={handleNewSearch}
        />
      )}
      {state === 'PickUp' && (
        <Map place={selectedPlace} handlePickupPoints={handlePickupPoints} />
      )}

      {dataToSend && (
        <Confirmation
          cardTitle={intl.formatMessage(confirm.pickupCardTitle)}
          cardSubtitle={dataToSend.Name}
          callback={handleSubmit}
          handleEdit={handleEdit}
        />
      )}
    </div>
  )
}
export default SecondTab
