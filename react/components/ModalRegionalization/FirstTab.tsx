/* eslint-disable */
import React, { useCallback, useState } from 'react'
import { useUpdateSession } from 'vtex.session-client'
import { useOrderForm } from 'vtex.order-manager/OrderForm'
import axios from 'axios'

import { Header } from './Header'
import { usePickup } from './hook/usePickup'
import { PlaceProps } from './types'
import { Confirmation } from './views/Confirmation'
import { SelectZipCode }  from './views/SelectZipCode'
import { useIntl } from 'react-intl'
import { confirm } from '../../utils/definedMessages'

import {useRuntime} from 'vtex.render-runtime'

const FirstTab = () => {
  const updateSession = useUpdateSession()
  const { toggleModal } = usePickup()
  const orderForm = useOrderForm()
  const {culture} = useRuntime()

  const intl = useIntl()
  const [place, setPlace] = useState<PlaceProps | null>(null)

  const handleSubmit = async (geoCoordinates: string, address: string) => {
    const response = await fetch(
      `/api/checkout/pub/regions?country=${culture.country}&geoCoordinates=${geoCoordinates}`
    )
    const data = await response.json()

    console.log('places: ', place)
    console.log('region id data: ', data[0].id)
    console.log('OF data: ', orderForm, useOrderForm)

    await fetch('/api/sessions', {
      method: 'POST',
      body: JSON.stringify({
        public: {
          regionId: {
            value: data?.[0].id,
          },
        },
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    }).then(() => {
      // const actualUrl = window.location.origin + window.location.pathname + window.location.search
      // window.location.replace(`${actualUrl}&regionId=${data?.[0].id}`)

      // // window.location.reload()
      RegionOF(place)
    })

    updateSession({
      variables: {
        fields: {
          Name: null,
          SellerId: null,
          postalCode: address,
          state: '',
          country: '',
        },
      },
      public: {
        regionId: {
          value: data?.[0].id,
        },
      },
    })

    toggleModal()
  }

  const handleSelect = (selectedPlace: PlaceProps) => {
    console.log('place: ', JSON.stringify(selectedPlace))

    console.log('lat: ', selectedPlace.geometry.location.lat())
    console.log('lng: ', selectedPlace.geometry.location.lng())
    setPlace(selectedPlace)
  }

  const handleConfirmation = useCallback(() => {
    handleSubmit(
      `${place?.geometry.location.lng()};${place?.geometry.location.lat()}`,
      place?.formatted_address ?? ''
    )
  }, [place])

  const handleEdit = () => {
    setPlace(null)
  }

  const RegionOF = (e) => {
    const body = {
      selectedAddresses: [
        {
          addressType: 'residential',
          geoCoordinates: [
            e.geometry.location.lng(),
            e.geometry.location.lat(),
          ],
          country: culture.country,
          street: e.address_components.filter((p) =>
            p.types.includes('route')
          )[0]?.long_name,
          city: e.address_components.filter((p) =>
            p.types.includes('locality')
          )[0]?.long_name,
          state: e.address_components.filter((p) =>
            p.types.includes('administrative_area_level_1')
          )[0]?.long_name,
        },
      ],
      // clearAddressIfPostalCodeNotFound: true,
    }

    if (orderForm) {
      axios
        .post(
          `/api/checkout/pub/orderForm/${orderForm?.orderForm?.id}/attachments/shippingData`,
          body
        )
        .then(() => {})
        .catch((err) => {
          console.log('error cartRegions', err)
        })
    }
  }

  return (
    <div>
      <Header />
      <SelectZipCode handleSelect={handleSelect} alternative />
      {place && (
        <Confirmation
          cardTitle={intl.formatMessage(confirm.deliveryCardTitle)}
          cardSubtitle={place.formatted_address}
          callback={handleConfirmation}
          handleEdit={handleEdit}
        />
      )}
    </div>
  )
}
export default FirstTab
