import React, { ChangeEvent } from 'react'
import { Spinner } from 'vtex.styleguide'

import {
  Form,
  FormContent,
  Button,
  Input,
  HeaderTitle,
  HeaderSecondTitle,
} from './styles'

interface ModalContainerProps {
  handleSubmit: React.FormEventHandler<HTMLFormElement>
  zipCode: string
  handleInputChange: (e: ChangeEvent<HTMLInputElement>) => void
  isLoading: boolean
}

const ModalContainer = ({
  handleSubmit,
  zipCode,
  handleInputChange,
  isLoading,
}: ModalContainerProps) => {
  return (
    <>
      <HeaderTitle>Home delivery</HeaderTitle>
      <HeaderSecondTitle>Enter your postal code</HeaderSecondTitle>
      <Form onSubmit={handleSubmit}>
        <FormContent>
          <Input
            id="zipCode"
            name="zipCode"
            maxLength={10}
            placeholder="1000"
            value={zipCode}
            onChange={handleInputChange}
            type="text"
            required
          />
          <Button disabled={isLoading} type="submit">
            <span className="absolute">
              {isLoading && <Spinner size={20} />}
            </span>
            <span className={`${isLoading && 'o-0'}`}>Submit</span>
          </Button>
        </FormContent>
      </Form>
    </>
  )
}
export default ModalContainer
