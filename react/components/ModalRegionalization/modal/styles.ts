import styled from 'styled-components'

export const Form = styled.form`
  display: flex;
  border: 0;
`

export const FormContent = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  border: 0;
  align-items: center;
`

export const Input = styled.input`
  background-color: #fdfdfd;
  border: 1px solid #e5e5e5;
  border-radius: 10px;
  height: 80px;
  max-height: 55px;
  font-size: 0.875rem;
  color: #0e3368;
  padding: 0 2rem;
  max-width: 65%;
`

export const Button = styled.button`
  border-radius: 0.25rem;
  cursor: pointer;
  color: #ffffff;
  text-align: center;
  font-weight: 600;
  font-size: 1rem;
  background-color: #ff3366;
  padding: 18px 37px;
  border: 0;
  width: 90%;
  margin: 1rem auto 0;
`
export const HeaderTitle = styled.span`
  font-size: 24px;
  margin: 0 0 5px;
  color: #333;
  font-weight: 700;
  text-align: center;
  display: flex;
  justify-content: center;
`
export const HeaderSecondTitle = styled.span`
  font-size: 1rem;
  color: #575757;
  padding: 0 20px;
  margin-bottom: 20px;
  text-align: center;
  display: flex;
  justify-content: center;
`
