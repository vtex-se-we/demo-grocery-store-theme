import React, { useContext, createContext, useCallback, useState } from 'react'

type PickupType = {
  isOpen: boolean
  closeModal: () => void
  openModal: () => void
  toggleModal: () => void
  handleTab1: () => void
  handleTab2: () => void
  handleGoBack: () => void
  activeTab: '' | 'tab1' | 'tab2'
}

const PickupContext = createContext({} as PickupType)

export const usePickup = () => {
  const pickup = useContext(PickupContext)
  if (!pickup) {
    throw new Error('usePickup must be used within a PickupContext')
  }
  return pickup
}

export const PickupProvider: React.FC = ({ children }) => {
  const [isOpen, setOpen] = useState(false)

  const closeModal = useCallback(() => {
    setOpen(false)
  }, [setOpen])
  const openModal = useCallback(() => {
    setOpen(true)
  }, [setOpen])
  const toggleModal = useCallback(() => {
    setOpen((old) => !old)
  }, [setOpen])

  const [activeTab, setActiveTab] = useState<PickupType['activeTab']>('')

  const handleTab1 = () => {
    setActiveTab('tab1')
  }

  const handleTab2 = () => {
    setActiveTab('tab2')
  }

  const handleGoBack = () => {
    setActiveTab('')
  }

  const provider: PickupType = {
    isOpen,
    closeModal,
    openModal,
    toggleModal,
    handleTab1,
    handleTab2,
    handleGoBack,
    activeTab,
  }

  return (
    <PickupContext.Provider value={provider}>{children}</PickupContext.Provider>
  )
}
