/* eslint-disable */
import React, { useEffect, useState } from 'react'
import { useCssHandles } from 'vtex.css-handles'
import { useIntl } from 'react-intl'
import { pickupPoints } from '../../../utils/definedMessages'

import { MarkerProps } from '../views/Map'
import {
  Card,
  CardFooter,
  CardSelectButton,
  HeaderTitle,
  List,
  NewSearchButton,
} from './styles'

// type pickupModal = {
//   id: string;
//   name: string;
//   city: string;
//   address: {
//     postalCode: string;
//     street: string;
//     number: string;
//   }
// }

type pickupModal = {
  SellerId: string
  Name: string
}

type sellerProps = {
  onClick: (SellerId: string, Name: string, geoCoordinates: string) => void
  handleNewSearch: () => void
  markers: MarkerProps[]
}

const CSS_HANDLES = ['containerListPickup', 'pickupList'] as const

function Pickup({ onClick, handleNewSearch, markers }: sellerProps) {
  const handles = useCssHandles(CSS_HANDLES)
  const [pickup, setPickup] = useState<pickupModal[]>([])
  // const [sellerName, setSellerName]: any = useState('')

  const intl = useIntl()
  useEffect(() => {
    fetch('/api/catalog_system/pvt/seller/list')
      .then((response) => response.json())
      .then((data) => {
        setPickup(data)
      })
  }, [])

  // console.log(sellerName, 'sellerName info')

  console.log('pickup: ', pickup)
  console.log('markers: ', markers)

  return (
    <>
      <HeaderTitle>{intl.formatMessage(pickupPoints.title)}</HeaderTitle>
      <List>
        {markers
          .map((seller) => {
            return (
              <Card
                key={seller.id}
                onClick={() =>
                  onClick(
                    seller.id.split('_')[0],
                    `${seller.pickupPoint.friendlyName} - ${
                      seller.id.split('_')[0]
                    }`,
                    `${seller.lng};${seller.lat}`
                  )
                }
                className={`${handles.pickupList}`}
                // style={{ marginBottom: 12, cursor: 'pointer' }}
              >
                <strong>{seller.pickupPoint.friendlyName}</strong>
                <span>{seller.pickupPoint.address.street}</span>
                <span>
                  {seller.pickupPoint.address.postalCode}{' '}
                  {seller.pickupPoint.address.city}
                </span>
                <span>{seller.id.split('_')[0]}</span>

                <CardFooter>
                  <CardSelectButton>{intl.formatMessage(pickupPoints.select)}</CardSelectButton>
                </CardFooter>
              </Card>
            )
          })}
      </List>
      <NewSearchButton onClick={handleNewSearch}>{intl.formatMessage(pickupPoints.button)}</NewSearchButton>
    </>
  )
}

export default Pickup
