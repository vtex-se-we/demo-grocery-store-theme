import styled from 'styled-components'

export const HeaderTitle = styled.span`
  font-size: 24px;
  margin: 0 0 5px;
  color: #333;
  font-weight: 700;
  text-align: center;
  display: flex;
  justify-content: center;
`

export const Card = styled.li`
  display: flex;
  flex-direction: column;
  cursor: pointer;
  box-sizing: border-box;
  min-width: 0px;
  appearance: none;
  line-height: inherit;
  text-decoration: none;
  font-size: inherit;
  color: #000;
  padding: 15px;
  box-shadow: rgb(0 0 0 / 10%) 0px 2px 5px;
  border-radius: 5px;
  margin: 10px auto;
  background-color: rgb(255, 255, 255);
  width: 90%;
  position: relative;
  text-align: left;
`

export const CardFooter = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-top: 15px;
`

export const CardSelectButton = styled.button`
  border-radius: 0.25rem;
  cursor: pointer;
  color: #ffffff;
  text-align: center;
  font-weight: 600;
  font-size: 1rem;
  background-color: #ff3366;
  padding: 13px 32px;
  border: 0;
  width: fit-content;
`
export const List = styled.ul`
  padding-left: 0;
  max-height: 86vh;
  overflow: auto;

  /* width */
  &::-webkit-scrollbar {
    width: 2px;
  }

  /* Track */
  &::-webkit-scrollbar-track {
    border-radius: 10px;
    box-shadow: inset 0 0 5px grey;
  }

  /* Handle */
  &::-webkit-scrollbar-thumb {
    background: var(--fs-color-main-0);
    border-radius: 10px;
  }
`

export const NewSearchButton = styled.button`
  background-color: #ff3366;
  color: #fff;
  font-weight: 600;
  font-size: 1rem;
  border-radius: 0.25rem;
  width: 90%;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0 auto;
  height: 50px;
  border: 0;
  margin-top: 1rem;
`
