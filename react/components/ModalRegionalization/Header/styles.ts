import styled from 'styled-components'

export const Container = styled.header`
  display: flex;
  justify-content: space-between;
  padding: 20px;
`
export const CloseButton = styled.button`
  display: flex;
  background: 0;
  border: 0;
  cursor: pointer;

  > svg {
    width: 15px;
    height: 22px;
    fill: #102134;
  }
`
export const ReturnButton = styled.button`
  display: flex;
  background: 0;
  border: 0;
  cursor: pointer;

  > svg {
    width: 27px;
    height: 22px;
    fill: #102134;
  }
`
