import React from 'react'

import { usePickup } from '../hook/usePickup'
import { Container, CloseButton, ReturnButton } from './styles'

export const Header = () => {
  const { closeModal, handleGoBack } = usePickup()
  return (
    <Container>
      <ReturnButton onClick={handleGoBack} title="Return" aria-label="Return">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 1.52 446.75 256.48">
          <path d="M2 138q2 5 6 9l105 105q6 6 15 6t15.5-6.5Q150 245 150 236t-6-15l-69-69 350 1q12 0 18.5-11t0-22q-6.5-11-18.5-11H74l70-70q8-9 5-21T134 2.5Q122-1 113 8L6 114q-4 5-5.5 11.5T2 138z"></path>
        </svg>
      </ReturnButton>

      <CloseButton
        onClick={closeModal}
        title="Close Modal"
        aria-label="Close Modal"
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="-0.25 1.75 256.25 256.25"
        >
          <path d="M102 129L5 32q-3-3-4.5-7.5t0-9q1.5-4.5 5-8t8-5q4.5-1.5 9 0T30 7l98 98 98-98q5-5 12.5-5T251 7q5 5 5 12.5T251 32l-97 97 97 99q5 5 5 12.5t-5 12.5q-5 5-12.5 5t-12.5-5l-98-99-98 99q-5 5-12.5 5T5 253q-5-5-5-12.5T5 228l97-99z"></path>
        </svg>
      </CloseButton>
    </Container>
  )
}
