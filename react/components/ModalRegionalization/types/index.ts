export type PlaceProps = {
  formatted_address: string
  geometry: {
    location: {
      lat: () => string
      lng: () => string
    }
  }
}
