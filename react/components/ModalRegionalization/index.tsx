/* eslint-disable */
import React, { useEffect, useState } from 'react'

// import { Modal } from 'vtex.styleguide'
import { useCssHandles } from 'vtex.css-handles'

import { Modal } from './SideBar'
// import ModalContainer from './modal'
import TabsModal from './TabsModal'
import './style.css'
import { PickupProvider, usePickup } from './hook/usePickup'
import { useIntl } from 'react-intl'
import { trigger } from '../../utils/definedMessages'


type sellerModal = {
  SellerId: string
  Name: string
}

const CSS_HANDLES = [
  'modalTitle',
  'regionalizationIcon',
  'description',
  'titleModal',
  'tabClassDelivery',
  'tabClassPickup',
  'barChange',
  'tabClassPickupC',
  'tabClassDeliveryC',
  'without'
] as const

const ModalRegionalization: React.FC = () => {
  const handles = useCssHandles(CSS_HANDLES)
  const [location, setLocation] = useState<PostalCodeInfo>() || null
  const [store, setStore] = useState<sellerModal>()
  const { isOpen, toggleModal } = usePickup()

  const intl = useIntl()
  const getPublicSessionInfo = async () => {
    const response = await fetch(
      `/api/sessions?items=public.country,public.postalCode,public.state,public.SellerId,public.Name`
    )
    const {
      namespaces: {
        public: { postalCode, country, state, SellerId, Name },
      },
    } = await response.json()

    if (SellerId && !postalCode) {
      setStore({
        SellerId: SellerId.value,
        Name: Name.value,
      })
    } else {
      setLocation({
        postalCode: postalCode?.value,
        country: country?.value,
        state: state?.value,
      })
    }
  }

  useEffect(() => {
    getPublicSessionInfo()
  }, [])
  // useEffect(() => {
  //   if (!location && !store) {
  //     openModal()
  //     return
  //   }
  //   closeModal()
  // }, [location, store])

  return (
    <>
      <div
        className={`${handles.barChange} white flex items-center justify-center`}
      >
        <div className={handles.regionalizationIcon}>
          {store || location ? (
            <svg
              width="15"
              height="15"
              viewBox="0 0 284 256"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M278 48L262 17C259.333 11.6667 255.5 7.5 250.5 4.5C245.5 1.5 240 0 234 0H50C44 0 38.5 1.5 33.5 4.5C28.5 7.5 24.6667 11.6667 22 17L5 48C1.66667 54.6667 0 61.5 0 68.5C0 75.5 1.16667 82 3.5 88C5.83333 94 9.33333 99.1667 14 103.5C18.6667 107.833 24 110.667 30 112L33 113V244C33 247.333 34.1667 250.167 36.5 252.5C38.8333 254.833 41.3333 256 44 256H239C242.333 256 245.167 254.833 247.5 252.5C249.833 250.167 251 247.333 251 244V113C257 111.667 262.5 109 267.5 105C272.5 101 276.5 96 279.5 90C282.5 84 284 77.1667 284 69.5C284 61.8333 282 54.6667 278 48ZM112 232H171V168H112V232ZM189 232V159C189 156.333 188 154.167 186 152.5C184 150.833 182 150 180 150H103C100.333 150 98.1667 150.833 96.5 152.5C94.8333 154.167 94 156.333 94 159V232H56V111C64 109 70.3333 105 75 99C83.6667 108.333 94.8333 113 108.5 113C122.167 113 133.333 108.333 142 99C150.667 108.333 161.833 113 175.5 113C189.167 113 200.333 108.333 209 99C213.667 104.333 219.667 108.333 227 111V232H189ZM257 80C254.333 85.3333 250 88.6667 244 90L242 89C235.333 89 230.167 87 226.5 83C222.833 79 221 73.3333 221 66C221 62.6667 219.667 59.8333 217 57.5C214.333 55.1667 211.5 54 208.5 54C205.5 54 202.833 55.1667 200.5 57.5C198.167 59.8333 197 62.6667 197 66C197 74.6667 194.333 81 189 85C185.667 87.6667 181.167 89 175.5 89C169.833 89 165.333 87.6667 162 85C156.667 81 154 74.6667 154 66C154 62.6667 152.833 60 150.5 58C148.167 56 145.333 55 142 55C138.667 55 135.833 56 133.5 58C131.167 60 130 62.6667 130 66C130 74.6667 127.333 81 122 85C118.667 87.6667 114.167 89 108.5 89C102.833 89 98.3333 87.6667 95 85C89.6667 81 87 74.6667 87 66C87 62.6667 85.8333 60 83.5 58C81.1667 56 78.5 55 75.5 55C72.5 55 69.8333 56 67.5 58C65.1667 60 63.6667 62.6667 63 66C63 73.3333 61.1667 79 57.5 83C53.8333 87 48.6667 89 42 89H37C31.6667 87.6667 27.8333 84 25.5 78C23.1667 72 23.6667 66 27 60L43 29C45 25.6667 47.3333 24 50 24H234C237.333 24 239.667 25.6667 241 29L257 59C259 62.3333 260 65.8333 260 69.5C260 73.1667 259 76.6667 257 80Z"
                fill="#fff"
              />
            </svg>
          ) : (
            <svg
              width="14"
              height="16"
              viewBox="0 0 194 256"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M53 95C53 107 57.1667 117.167 65.5 125.5C73.8333 133.833 83.8333 138 95.5 138C107.167 138 117.167 133.833 125.5 125.5C133.833 117.167 138 107.167 138 95.5C138 83.8333 133.833 73.8333 125.5 65.5C117.167 57.1667 107.167 53 95.5 53C83.8333 53 73.8333 57.1667 65.5 65.5C57.1667 73.8333 53 83.6667 53 95ZM72 95C72 89 74.3333 83.6667 79 79C83.6667 74.3333 89.1667 72 95.5 72C101.833 72 107.167 74.3333 111.5 79C115.833 83.6667 118 89.1667 118 95.5C118 101.833 115.833 107.167 111.5 111.5C107.167 115.833 101.833 118 95.5 118C89.1667 118 83.6667 115.833 79 111.5C74.3333 107.167 72 101.667 72 95ZM86 251C88.6667 254.333 92.3333 256 97 256C101.667 256 105.333 254.333 108 251C120 237 131.667 222 143 206C161.667 180.667 175.667 157.333 185 136C191 124 194 111 194 97C194 79.6667 189.667 63.5 181 48.5C172.333 33.5 160.5 21.6667 145.5 13C130.5 4.33333 114.333 0 97 0C79.6667 0 63.5 4.33333 48.5 13C33.5 21.6667 21.6667 33.5 13 48.5C4.33333 63.5 0 79.6667 0 97C0 111 3 124 9 136C18.3333 157.333 32.3333 181 51 207C62.3333 222.333 74 237 86 251ZM20 97C20 83 23.3333 70 30 58C36.6667 46 46 36.6667 58 30C70 23.3333 83 20 97 20C111 20 123.833 23.3333 135.5 30C147.167 36.6667 156.5 46 163.5 58C170.5 70 174 83 174 97C174 108.333 171.667 118.667 167 128C159 146.667 147 167.333 131 190C120.333 204.667 109 219.333 97 234C85 219.333 73.6667 204.667 63 190C47 167.333 34.6667 146.667 26 128C22 118 20 107.667 20 97Z"
                fill="#fff"
              />
            </svg>
          )}
        </div>
        <p
          onClick={toggleModal}
          className={`${handles.modalTitle} f7 b ml2 pointer`}
        >
          {console.log(store, location, "querooooooo")}
          {store && !location?.postalCode ? (
            <><span className={`${handles.tabClassDelivery}`}>
              {intl.formatMessage(trigger.pickup)}
            </span><span>{store?.Name}</span><span className={`${handles.tabClassDeliveryC}`}>{intl.formatMessage(trigger.change)}</span></>
          ) : !store && location?.postalCode ? (
            <><span className={`${handles.tabClassPickup}`}>
            {intl.formatMessage(trigger.delivery)}
            </span><span>{location?.postalCode}</span><span className={`${handles.tabClassPickupC}`}>{intl.formatMessage(trigger.change)}</span></>
          ) : (
            <div className={`${handles.without}`}>
              <span>{intl.formatMessage(trigger.title)}</span>
              <span className={handles.description}>
              {intl.formatMessage(trigger.subtitle)}
              </span>
            </div>
          )}
        </p>
      </div>

      <Modal isOpen={isOpen} onClose={toggleModal}>
        {/* <label htmlFor="zipCode" className={`${handles.titleModal}`}>
        Your order<br />
How would you like to pick up your order?
        </label> */}
        <TabsModal />
      </Modal>
    </>
  )
}

const ModalWithContext: React.FC = () => {
  return (
    <PickupProvider>
      <ModalRegionalization />
    </PickupProvider>
  )
}

export default ModalWithContext
