/* eslint-disable */
import React, { Fragment, useCallback, useEffect, VFC } from 'react'
import ReactDOM from 'react-dom'
import {
  GoogleMap as GoogleMapOld,
  useJsApiLoader,
  Marker as MarkerOld,
} from '@react-google-maps/api'
import { useRuntime } from 'vtex.render-runtime'

import { PlaceProps } from '../../types'

const containerStyle = {
  width: '100%',
  height: '100%',
}

const GoogleMap = GoogleMapOld as any
const Marker = MarkerOld as any

type PickUpPoint = {
  items: {
    distance: number
    pickupPoint: {
      address: {
        addressType: string
        // receiverName: null,
        addressId: string
        isDisposable: boolean
        postalCode: string
        city: string
        state: string
        country: string
        street: string
        // number: null,
        // neighborhood: null,
        // complement: null,
        // reference: null,
        geoCoordinates: number[]
      }
      friendlyName: string
      id: string
    }
  }[]
}

export type MarkerProps = {
  id: string
  lat: number
  lng: number
  pickupPoint: PickUpPoint['items'][number]['pickupPoint']
}

type MapProps = {
  place: PlaceProps
  handlePickupPoints: (markers: MarkerProps[]) => void
}

const MapComponent: VFC<MapProps> = ({ place, handlePickupPoints }) => {
  console.log('map place: ', place)
  const {culture} = useRuntime()
  const [map, setMap] = React.useState(null)
  const [markers, setMarkers] = React.useState<MarkerProps[]>([])

  console.log('google map: ', map)
  ;(window as any).testvalue = place

  const onLoad = React.useCallback(function callback(map) {
    setMap(map)
  }, [])

  const onUnmount = React.useCallback(function callback() {
    setMap(null)
  }, [])

  const { isLoaded } = useJsApiLoader({
    id: 'google-map-script',
    googleMapsApiKey: 'AIzaSyA6grIoeBOILttVg6r9s_yR8ENCS50FvmY',
  })

  const getPickUpPoints = useCallback(async () => {
    if (!place.geometry) {
     
      return
    }

   
    const response = await fetch(
      `/api/checkout/pub/pickup-points?countryCode=${culture.country}&geoCoordinates=${place.geometry.location.lng()};${place.geometry.location.lat()}`
    )
    const data: PickUpPoint = await response.json()

    console.log('pickup points: ', data)

    if (!data) {
      return
    }

    const markers = data?.items?.map((item) => {
      return {
        id: item.pickupPoint.id,
        lat: item.pickupPoint.address.geoCoordinates[1],
        lng: item.pickupPoint.address.geoCoordinates[0],
        pickupPoint: item.pickupPoint,
      }
    })

    setMarkers(markers)
    handlePickupPoints(markers)
  }, [place])

  useEffect(() => {
    getPickUpPoints()
  }, [place])

  return isLoaded && !!place.geometry ? (
    <GoogleMap
      mapContainerStyle={containerStyle}
      center={{
        lat: place.geometry.location.lat(),
        lng: place.geometry.location.lng(),
      }}
      zoom={10}
      onLoad={onLoad}
      onUnmount={onUnmount}
    >
      {/* Child components, such as markers, info windows, etc. */}
      <>
        {markers.map((marker) => {
          return (
            <Fragment key={marker.id}>
              <Marker
                position={{
                  lat: Number(marker.lat),
                  lng: Number(marker.lng),
                }}
                title={marker.pickupPoint.friendlyName}
                // icon="path/file.svg"
              />
            </Fragment>
          )
        })}
      </>
    </GoogleMap>
  ) : (
    <></>
  )
}

export const Map: VFC<MapProps> = ({ place, handlePickupPoints }) => {
  const element = document.querySelector('#map-wrapper-regionalization')
  if (!element) {
    return <></>
  }

  return ReactDOM.createPortal(
    <MapComponent place={place} handlePickupPoints={handlePickupPoints} />,
    element
  )
}
