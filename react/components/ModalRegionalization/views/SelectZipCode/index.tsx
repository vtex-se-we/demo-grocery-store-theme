import type { VFC } from 'react'
import React from 'react'
import { useIntl } from 'react-intl'
import { useRuntime } from 'vtex.render-runtime'

import {
  insertDelivery,
  insertPickup,
  insert,
} from '../../../../utils/definedMessages'
import {
  Container,
  Header,
  FirstTitle,
  SecondTitle,
  AutoCompleteStyled,
  Content,
} from './styles'
import type { PlaceProps } from '../../types'

type SelectZipCodeProps = {
  handleSelect: (place: PlaceProps) => void
  alternative?: boolean
}

export const SelectZipCode: VFC<SelectZipCodeProps> = ({
  handleSelect,
  alternative,
}) => {
  const intl = useIntl()
  const { culture } = useRuntime()

  const countryISOMapping = {
    GBR: 'GB',
    ESP: 'ES',
    ITA: 'IT',
  }

  return (
    <Container>
      <Header>
        <FirstTitle>
          {alternative
            ? intl.formatMessage(insertDelivery.title)
            : intl.formatMessage(insertPickup.title)}
        </FirstTitle>
        <SecondTitle>
          {alternative
            ? intl.formatMessage(insertDelivery.subtitle)
            : intl.formatMessage(insertPickup.subtitle)}
        </SecondTitle>
      </Header>

      <Content>
        <AutoCompleteStyled
          apiKey="AIzaSyA6grIoeBOILttVg6r9s_yR8ENCS50FvmY"
          onPlaceSelected={(place: any) => handleSelect(place)}
          options={{
            types: ['street_address', 'street_number', 'route'],
            componentRestrictions: {
              country: countryISOMapping[culture.country],
            },
          }}
          placeholder={intl.formatMessage(insert.placeholder)}
        />
      </Content>
      <style>
        {`
            .pac-container {
              z-index: 1020;
            }
          `}
      </style>
    </Container>
  )
}
