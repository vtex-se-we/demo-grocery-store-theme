import styled from 'styled-components'
import Autocomplete from 'react-google-autocomplete'

export const Container = styled.div``
export const Header = styled.header`
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const FirstTitle = styled.span`
  font-size: 24px;
  margin: 0 0 5px;
  color: #333;
  font-weight: 700;
  text-align: center;
`

export const SecondTitle = styled.span`
  font-size: 1rem;
  color: #575757;
  padding: 0 20px;
  margin-bottom: 20px;
  text-align: center;
`

export const Content = styled.div`
  display: flex;
  justify-content: center;
`

export const AutoCompleteStyled = styled(Autocomplete)`
  background-color: #fdfdfd;
  border: 1px solid #e5e5e5;
  border-radius: 10px;
  height: 80px;
  max-height: 55px;
  font-size: 0.875rem;
  color: #0e3368;
  padding: 0 2rem;
  max-width: 65%;
`
