import styled from 'styled-components'

export const Container = styled.div`
  position: absolute;
  inset: 0;
  background-color: #f7f7f7;
`

export const Button = styled.button`
  background-color: #ff3366;
  color: #fff;
  font-weight: 600;
  font-size: 1rem;
  border-radius: 0.25rem;
  width: 90%;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0 auto;
  height: 50px;
  border: 0;
  margin-top: 1rem;
`

export const ConfirmationHeader = styled.header`
  display: flex;
  flex-direction: column;
  align-items: center;
`

export const FirstTitle = styled.span`
  font-size: 24px;
  margin: 0 0 5px;
  color: #333;
  font-weight: 700;
  text-align: center;
`

export const SecondTitle = styled.span`
  font-size: 1rem;
  color: #575757;
  padding: 0 20px;
  margin-bottom: 20px;
  text-align: center;
  font-weight: 400;
`

export const Card = styled.div`
  background-color: #ffffff;
  border: 1px solid #e6e6e6;
  border-radius: 5px;
  display: flex;
  justify-content: flex-start;
  padding: 20px;
  width: 90%;
  margin: 0 auto;

  > svg {
    width: 30px;
    height: 40px;
    margin-right: 20px;
    fill: #0e3368;
  }
`

export const CardTextContainer = styled.div`
  display: flex;
  flex-direction: column;
`

export const CardTitle = styled.span`
  color: #333;
  font-weight: 700;
  margin-bottom: 5px;
  font-size: 14px;
`

export const CardSubtitle = styled.span`
  color: #575757;
  font-weight: 700;
  margin-bottom: 10px;
  font-size: 14px;
`

export const CardEditButton = styled.button`
  color: #102134;
  background: transparent;
  border: 0;
  display: flex;
  justify-content: flex-start;
  cursor: pointer;

  &::before {
    content: '';
    background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='25.2 24 213.8 213' fill='%23102134'%3E%3Cpath d='M195 28q-4-4-9.5-4t-9.5 4L40 164q-3 4-3 7l-11 51q-2 5 1 9.5t8 5.5h6l51-11q5-2 7-4L235 86q4-3 4-9t-3-9l-41-40zM86 207l-37 7 7-37 97-96 30 29-97 97z'%3E%3C/path%3E%3C/svg%3E");
    background-repeat: no-repeat;
    background-position: center;
    margin-right: 4px;
    display: flex;
    width: 10px;
    height: 15px;
  }
`
