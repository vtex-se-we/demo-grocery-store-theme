import React, { VFC } from 'react'
import ReactDOM from 'react-dom'
import { useIntl } from 'react-intl'

import { confirm } from '../../../../utils/definedMessages'
import { Header } from '../../Header'
import {
  Container,
  Button,
  ConfirmationHeader,
  FirstTitle,
  SecondTitle,
  Card,
  CardTitle,
  CardSubtitle,
  CardTextContainer,
  CardEditButton,
} from './styles'

type ConfirmationProps = {
  callback: () => void
  handleEdit: () => void
  cardTitle: string
  cardSubtitle: string
}

const ConfirmationComponent: VFC<ConfirmationProps> = ({
  callback,
  handleEdit,
  cardTitle,
  cardSubtitle,
}) => {
  const intl = useIntl()
  return (
    <Container>
      <Header />
      <ConfirmationHeader>
        <FirstTitle>{intl.formatMessage(confirm.title)}</FirstTitle>
        <SecondTitle>{intl.formatMessage(confirm.subtitle)}</SecondTitle>
      </ConfirmationHeader>
      <Card>
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 6 1135 1024">
          <path d="M1114 199l-65-124q-17-32-47.5-50.5T936 6H200q-35 0-65.5 19T87 76L22 200q-11 19-16.5 41T0 287q0 20 4 39t11 36v-2q15 36 42.5 61t62.5 35q3 0 5.5.5t4.5.5v525q0 20 14 34t34 14h779q20 0 34-14t14-34V456q37-8 65.5-31t44.5-55q10-19 15-40t5-44q0-24-5.5-46t-16.5-42l1 1zM449 934h234V678H449v256zm306 0V642q0-15-11-25.5T719 606H413q-15 0-25.5 10.5T377 642v292H226V450q22-6 41.5-18t34.5-28q24 25 57.5 39t75.5 14q41 0 75-14t58-40q24 26 58 40t75 14q42 0 75.5-14t57.5-40q15 17 34 28.5t41 18.5v484H755zm275-608q-5 10-17 22.5T977 364h-10q-40 0-62.5-24.5T882 271q0-20-14-34t-34-14q-20 0-34 14t-14 33v1q0 59-30.5 74.5T701 361q-24 0-54.5-15.5T616 271q-1-20-15-33.5T568 224q-20 0-33.5 13.5T520 270v1q0 59-30.5 74.5T435 361q-24 0-54.5-15.5T350 271q-1-20-15-33t-33-13q-20 0-33.5 13T254 270v1q0 44-22.5 68.5T169 364h-16.5q-4.5 0-6.5-1-19-5-29-18t-14-22q-4-8-5.5-17T96 287q0-11 2.5-22t7.5-21v1l66-124q4-9 12-14t16-5h736q8 0 16 5t12 13l65 124q10 19 10.5 41t-9.5 41z"></path>
        </svg>
        <CardTextContainer>
          <CardTitle>{cardTitle}</CardTitle>
          <CardSubtitle>{cardSubtitle}</CardSubtitle>
          <CardEditButton onClick={handleEdit}>
            {intl.formatMessage(confirm.cardEdit)}
          </CardEditButton>
        </CardTextContainer>
      </Card>
      <Button onClick={callback}>
        {intl.formatMessage(confirm.confirmButton)}
      </Button>
    </Container>
  )
}

export const Confirmation: VFC<ConfirmationProps> = ({
  callback,
  handleEdit,
  cardTitle,
  cardSubtitle,
}) => {
  const target = document.querySelector('#confirmation-wrapper-regionalization')

  if (!target) {
    return <></>
  }

  return ReactDOM.createPortal(
    <ConfirmationComponent
      callback={callback}
      handleEdit={handleEdit}
      cardTitle={cardTitle}
      cardSubtitle={cardSubtitle}
    />,
    target
  )
}
