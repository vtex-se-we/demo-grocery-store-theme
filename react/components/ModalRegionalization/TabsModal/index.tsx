import React from 'react'
import { useCssHandles } from 'vtex.css-handles'
import { useIntl } from 'react-intl'

import { modalHome } from '../../../utils/definedMessages'
import FirstTab from '../FirstTab'
import SecondTab from '../SecondTab'
import {
  Container,
  TabContainer,
  Card,
  CardContainer,
  CardTitle,
  CardSecondTitle,
  Header,
  HeaderTitle,
  HeaderSecondTitle,
} from './styles'
import { usePickup } from '../hook/usePickup'

const CSS_HANDLES = [
  'navModal',
  'delivery',
  'pickup',
  'active1',
  'active2',
  'tabLayout',
] as const

const TabsModal = () => {
  const handles = useCssHandles(CSS_HANDLES)
  const { activeTab, handleTab1, handleTab2 } = usePickup()
  const intl = useIntl()

  return (
    <Container>
      {!activeTab && (
        <Header>
          <HeaderTitle>{intl.formatMessage(modalHome.titleModal)}</HeaderTitle>
          <HeaderSecondTitle>
            {intl.formatMessage(modalHome.subtitleModal)}
          </HeaderSecondTitle>
        </Header>
      )}
      <CardContainer className={`${handles.navModal}`}>
        <Card
          className={
            activeTab === 'tab1' ? `${handles.active1}` : `${handles.delivery}`
          }
          onClick={handleTab1}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            viewBox="-0.67 6 1112.67 1024"
          >
            <path d="M1111 454q-5-24-16.5-43.5T1066 377L653 40q-21-17-46-25.5T556 6q-26 0-50.5 8.5T460 40L44 379q-15 12-26 28T3 441q-13 63 20 107t83 53v320q0 45 32 77t77 32h682q46 0 78-32t32-77V601q45-8 75-43.5t30-82.5q0-5-.5-11t-1.5-11l1 1zm-118 67l-68 11v389q0 11-8 19.5t-20 8.5H692V755q-1-21-16-36.5T640 703H473q-22 0-37 15.5T421 755v194H215q-12 0-20-8.5t-8-19.5V532l-67-12q-10-1-17.5-6T90 501h-1q-5-8-7-19t1-24q1-2 3.5-6.5T95 442l416-339q9-7 20.5-11.5T556 87q13 0 24.5 4.5T601 104v-1l413 337q8 6 12.5 15.5t4.5 19.5q0 17-11 30t-27 16z" />
          </svg>
          <CardTitle>
            {intl.formatMessage(modalHome.deliveryCardTitle)}
          </CardTitle>
          <CardSecondTitle>
            {intl.formatMessage(modalHome.deliveryCardSubtitle)}
          </CardSecondTitle>
        </Card>
        <Card
          className={
            activeTab === 'tab2' ? `${handles.active2}` : `${handles.pickup}`
          }
          onClick={handleTab2}
        >
          <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 6 1135 1024">
            <path d="M1114 199l-65-124q-17-32-47.5-50.5T936 6H200q-35 0-65.5 19T87 76L22 200q-11 19-16.5 41T0 287q0 20 4 39t11 36v-2q15 36 42.5 61t62.5 35q3 0 5.5.5t4.5.5v525q0 20 14 34t34 14h779q20 0 34-14t14-34V456q37-8 65.5-31t44.5-55q10-19 15-40t5-44q0-24-5.5-46t-16.5-42l1 1zM449 934h234V678H449v256zm306 0V642q0-15-11-25.5T719 606H413q-15 0-25.5 10.5T377 642v292H226V450q22-6 41.5-18t34.5-28q24 25 57.5 39t75.5 14q41 0 75-14t58-40q24 26 58 40t75 14q42 0 75.5-14t57.5-40q15 17 34 28.5t41 18.5v484H755zm275-608q-5 10-17 22.5T977 364h-10q-40 0-62.5-24.5T882 271q0-20-14-34t-34-14q-20 0-34 14t-14 33v1q0 59-30.5 74.5T701 361q-24 0-54.5-15.5T616 271q-1-20-15-33.5T568 224q-20 0-33.5 13.5T520 270v1q0 59-30.5 74.5T435 361q-24 0-54.5-15.5T350 271q-1-20-15-33t-33-13q-20 0-33.5 13T254 270v1q0 44-22.5 68.5T169 364h-16.5q-4.5 0-6.5-1-19-5-29-18t-14-22q-4-8-5.5-17T96 287q0-11 2.5-22t7.5-21v1l66-124q4-9 12-14t16-5h736q8 0 16 5t12 13l65 124q10 19 10.5 41t-9.5 41z" />
          </svg>
          <CardTitle>{intl.formatMessage(modalHome.pickupCardTitle)}</CardTitle>
          <CardSecondTitle>
            {intl.formatMessage(modalHome.pickupCardSubtitle)}
          </CardSecondTitle>
        </Card>
      </CardContainer>
      {!!activeTab && (
        <TabContainer className={`${handles.tabLayout}`}>
          {/* <button onClick={handleGoBack}>go back</button> */}
          {activeTab === 'tab1' ? <FirstTab /> : <SecondTab />}
        </TabContainer>
      )}
    </Container>
  )
}

export default TabsModal
