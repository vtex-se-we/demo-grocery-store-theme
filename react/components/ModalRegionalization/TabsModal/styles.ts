import styled from 'styled-components'

export const Container = styled.div`
  position: relative;
  padding-left: 20px;
  padding-right: 20px;
  height: 100%;
`

export const Header = styled.header`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 62px;
`

export const HeaderTitle = styled.span`
  font-size: 24px;
  margin: 0 0 5px;
  color: #333;
  font-weight: 700;
  text-align: center;
`

export const HeaderSecondTitle = styled.span`
  font-size: 1rem;
  color: #575757;
  padding: 0 20px;
  margin-bottom: 20px;
  text-align: center;
`

export const TabContainer = styled.div`
  position: absolute;
  inset: 0;
  background-color: #f7f7f7;
`

export const Card = styled.button`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px;
  margin-bottom: 10px;
  background-color: #fff;
  box-shadow: 0 0 10px rgb(0 0 0 / 8%);
  border-radius: 6px;
  border: 0;
  cursor: pointer;

  &:hover {
    background-color: #e4f0fe;
  }

  &::before {
    display: none;
  }

  > svg {
    max-width: 45px;
    max-height: 45px;
    fill: #102134;
  }
`
export const CardContainer = styled.div`
  display: flex;
  flex-direction: column;
`
export const CardTitle = styled.span`
  font-size: 14px;
  margin-bottom: 10px;
  margin-top: 0;
  font-weight: 700;
  color: #333;
`

export const CardSecondTitle = styled.span`
  font-size: 12px;
  color: #575757;
  padding: 0 20px;
`
