/* eslint-disable object-curly-newline */
/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable semi */
import React, { useEffect } from 'react'
import { Button } from 'vtex.styleguide'
import { useLazyQuery } from 'react-apollo'

import MoreStoresContainer from './MoreStoresContainer'
import { IconPickup } from '../../assets'
import styles from './styles.css'
import PICKUP_POINTS from './graphql/PICKUP_POINTS.gql'

interface IPickupDataProps {
  pickupInfo: any
  setOrderDeliveryType: (arg: 'pickup' | 'delivery' | null) => void
  orderDeliveryType: 'pickup' | 'delivery' | null
  setFinalPickup: (arg: any) => void
}

const PickupData: React.FunctionComponent<IPickupDataProps> = ({
  pickupInfo,
  setOrderDeliveryType,
  orderDeliveryType,
  setFinalPickup,
}) => {
  const [searchPickupPoint, { data: dataPickup }] = useLazyQuery(
    PICKUP_POINTS,
    {
      fetchPolicy: 'cache-and-network',
    }
  )

  if (!pickupInfo || !pickupInfo?.pickupStoreInfo?.address?.geoCoordinates) {
    return null
  }

  const long = pickupInfo?.pickupStoreInfo?.address?.geoCoordinates?.[0]
  const lat = pickupInfo?.pickupStoreInfo?.address?.geoCoordinates?.[1]

  useEffect(() => {
    searchPickupPoint({ variables: { long, lat } })
  }, [long, lat])

  const {
    pickupStoreInfo: {
      address: { street, number, neighborhood, city },
    },
    friendlyName,
  } = pickupInfo

  return (
    <div
      key={pickupInfo.id}
      className={`${styles.infoCards} ${
        orderDeliveryType === 'pickup' ? styles.deliveryTypeSelected : ''
      }`}
    >
      <Button onClick={() => setOrderDeliveryType('pickup')}>
        <div className={styles.infoCardsContent}>
          <div className={styles.infoCardsContentTitle}>
            <IconPickup />
            <h3>Retirar na Loja</h3>
          </div>
          <div className={`${styles.infoCardsContentDelivery} mb4`}>
            <span>Frete Grátis</span>
          </div>
          <div className={`${styles.infoCardsContentStore} b ttn tl`}>
            {dataPickup?.searchPickupPoint.items.length > 1 ? (
              <MoreStoresContainer
                orderDeliveryType={orderDeliveryType}
                setOrderDeliveryType={setOrderDeliveryType}
                pickupPoints={dataPickup?.searchPickupPoint.items}
                setFinalPickup={setFinalPickup}
              />
            ) : (
              <>
                <div className="f7 mb1 tc">{friendlyName}</div>
                <div className={`${styles.pickupAddressInfo} tc`}>
                  {`${street}, ${number} - ${neighborhood}, ${city}`}
                </div>
              </>
            )}
          </div>
        </div>
      </Button>
    </div>
  )
}

export default PickupData
