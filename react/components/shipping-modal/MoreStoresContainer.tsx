import React, { useState, FC, useEffect } from 'react'

import { IconRadio } from '../../assets'
import styles from './styles.css'

interface IMoreStoresModalProps {
  pickupPoints: any[]
  setFinalPickup: (arg: any) => void
  setOrderDeliveryType: (arg: 'pickup' | 'delivery' | null) => void
  orderDeliveryType: 'pickup' | 'delivery' | null
}

const MoreStoresContainer: FC<IMoreStoresModalProps> = ({
  pickupPoints,
  setFinalPickup,
  orderDeliveryType,
  setOrderDeliveryType,
}) => {
  const storagePickupId = localStorage.getItem('@mambo-store-id')
  const [idSelected, setIdSelected] = useState(
    storagePickupId ?? pickupPoints[0].pickupPoint.id
  )

  useEffect(() => {
    const isPickup = localStorage.getItem('@mambo-pickup')

    if (isPickup) {
      setOrderDeliveryType('pickup')
    }
  }, [])

  return (
    <>
      <div className={`${styles.selectStoreContent}`}>
        {pickupPoints.map((item) => {
          const finalPickup = {
            id: item.pickupPoint.id,
            pickupPointId: item.pickupPoint.id,
            deliveryChannel: 'pickup-in-point',
            friendlyName: item.pickupPoint.friendlyName,
            pickupStoreInfo: {
              address: item.pickupPoint.address,
              friendlyName: item.pickupPoint.friendlyName,
            },
            shippingEstimate: '4h',
          }

          return (
            <>
              <button
                type="button"
                onClick={() => {
                  setFinalPickup(finalPickup)
                  setIdSelected(finalPickup.id)
                }}
                className={`${styles.pickupStore} mb4 items-center`}
              >
                <div className="flex mb1 items-center">
                  <div className="mr3 flex">
                    <IconRadio
                      color={
                        idSelected === item.pickupPoint.id &&
                        orderDeliveryType === 'pickup'
                          ? '#F44237'
                          : '#FFF'
                      }
                    />
                  </div>
                  <div className={styles.pickupStoreContent}>
                    <h3 className={`${styles.pickupStoreContentTitle} b f7`}>
                      {finalPickup.friendlyName}
                    </h3>
                  </div>
                </div>
                <div>
                  <div className={`${styles.pickupAddressInfo} tl`}>
                    {`${item.pickupPoint.address.street}, ${item.pickupPoint.address.number} - ${item.pickupPoint.address.neighborhood}, ${item.pickupPoint.address.city}`}
                  </div>
                </div>
              </button>
            </>
          )
        })}
      </div>
    </>
  )
}

export default MoreStoresContainer
