/* eslint-disable react/jsx-pascal-case */
import React, {
  FunctionComponent,
  useCallback,
  useEffect,
  useState,
} from 'react'
import { useLazyQuery } from 'react-apollo'
import {
  AddressContainer,
  AddressRules,
  PostalCodeGetter,
} from 'vtex.address-form'
import { addValidation } from 'vtex.address-form/helpers'
import { StyleguideInput } from 'vtex.address-form/inputs'
import { useCssHandles } from 'vtex.css-handles'
import { OrderShippingProvider } from 'vtex.order-shipping/OrderShipping'
import { useRuntime } from 'vtex.render-runtime'
import { EXPERIMENTAL_Modal, Spinner } from 'vtex.styleguide'

import CompleteAddress from './CompleteAddress'
import SIMULATE_SHIPPING from './graphql/SIMULATE_SHIPPING.gql'
import PICKUP_POINTS from './graphql/PICKUP_POINTS.gql'
import newAddressHelper from './newAddressHelper'
import styles from './styles.css'
import './styles.global.css'

const SIMULATE_SHIPPING_CSS_HANDLES = [
  'simulateShippingContainer',
  'containerNotAttend',
  'savedAddressButton',
  'contentTryAgain',
  'contentOops',
  'contentTitleTryAgain',
]

interface ShippingModalProps {
  isOpenModal: boolean
  onClose: () => void
}

const ShippingModal: FunctionComponent<ShippingModalProps> = ({
  isOpenModal,
  onClose,
}) => {
  const handles = useCssHandles(SIMULATE_SHIPPING_CSS_HANDLES)

  const [postalCodeError, setPostalCodeError] = useState(false)
  const [isValidPostalCode, setValidPostalCode] = useState(false)

  const {
    culture: { country },
  } = useRuntime()

  const resetAddress = () => addValidation(newAddressHelper(country))
  const [address, setAddress] = useState<AddressWithValidation>(resetAddress)

  const [simulateShipping, { loading, error, data, called }] = useLazyQuery(
    SIMULATE_SHIPPING,
    {
      fetchPolicy: 'cache-and-network',
    }
  )

  const [searchPickupPoint, { loading: loadingPickup, data: dataPickup }] =
    useLazyQuery(PICKUP_POINTS, {
      fetchPolicy: 'cache-and-network',
    })

  const handleResetAddress = () => {
    setAddress(resetAddress())
    setPostalCodeError(false)
    setValidPostalCode(false)
  }

  const getPickup = (long, lat) => {
    searchPickupPoint({ variables: { long, lat } })
  }

  const submitPostalCode = useCallback(
    (newAddress?: AddressWithValidation, shouldUpdate?: boolean) => {
      const localAddress = newAddress ?? address

      if (
        localAddress.postalCode.valid &&
        localAddress.postalCode.value &&
        !localAddress.postalCode.loading
      ) {
        const postalCode = localAddress.postalCode.value

        simulateShipping({ variables: { country, postalCode } })

        if (shouldUpdate) {
          setAddress(localAddress)
        }

        getPickup(
          localAddress.geoCoordinates?.value?.[0],
          localAddress.geoCoordinates?.value?.[1]
        )

        return
      }

      const updatedAddress = {
        ...localAddress,
        postalCode: {
          ...localAddress.postalCode,
          reason: !localAddress.postalCode.value
            ? 'ERROR_EMPTY_FIELD'
            : undefined,
        },
      }

      setAddress(updatedAddress)
    },
    [address, simulateShipping, country, setAddress]
  )

  const handleAddressChange = useCallback(
    (newAddress: AddressWithValidation) => {
      const updatedAddress = {
        ...address,
        ...newAddress,
      }

      setAddress(updatedAddress)
    },

    [address, setAddress]
  )

  useEffect(() => {
    const localAddress = localStorage.getItem('@store-address')

    if (localAddress) {
      submitPostalCode(JSON.parse(localAddress) as AddressWithValidation, true)
    }
  }, [])

  useEffect(() => {
    if (loading || error || !data?.shipping || !called || loadingPickup) {
      return
    }

    if (!address.street.postalCodeAutoCompleted) {
      setPostalCodeError(true)
    }

    const {
      logisticsInfo: [logisticInfo],
    } = data.shipping

    if (
      !logisticInfo?.slas.length &&
      !dataPickup?.searchPickupPoint?.items.length
    ) {
      setPostalCodeError(true)
    }

    localStorage.setItem('@store-address', JSON.stringify(address))
    setValidPostalCode(true)
    localStorage.setItem(
      '@store-valid',
      JSON.stringify(address.city.valid)
    )
  }, [loading, data, dataPickup, loadingPickup])

  return (
    <OrderShippingProvider>
      <EXPERIMENTAL_Modal
        isOpen={isOpenModal}
        centered
        showBottomBarBorder={false}
        showTopBar={false}
        closeOnOverlayClick
        closeOnEsc={false}
        showCloseIcon
        onClose={onClose}
        size="auto"
      >
        <AddressRules country={country} shouldUseIOFetching>
          <AddressContainer
            Input={StyleguideInput}
            address={address}
            onChangeAddress={handleAddressChange}
          >
            {postalCodeError ? (
              <div className={`${handles.containerNotAttend} tc pa6 mw5`}>
                <p
                  className={`${handles.contentOops} f1 fw8 mt0 mb0`}
                  style={{ color: '#FF863F' }}
                >
                  Oops!
                </p>
                <p className={`${handles.contentTitleTryAgain} f4 fw8 mt0 mb0`}>
                  Ainda não atendemos esta região.
                </p>
                <p className={`${handles.contentTryAgain} f6 mt5`}>
                  Tente novamente usando outro CEP.
                </p>
                <div className="mt5">
                  <button
                    type="button"
                    className={styles.infoCardsContentButton}
                    onClick={() => handleResetAddress()}
                  >
                    <span className="ttn pt2 white fw7">Usar outro CEP</span>
                  </button>
                </div>
              </div>
            ) : (
              <div
                className={`${handles.simulateShippingContainer} center pb3 ph6 ph7-ns pt0`}
              >
                {!isValidPostalCode ? (
                  <>
                    {loading ? (
                      <div className="pa10 tc w-100">
                        <Spinner size={40} />
                      </div>
                    ) : (
                      <div className={styles.content}>
                        <div className={styles.header}>
                          <h1 className={`${styles.title} mb0`}>
                            Digite seu CEP
                          </h1>
                        </div>
                        <p className={styles.description}>
                          Informe o seu CEP para saber se atendemos sua região.
                        </p>
                        <div className={styles.containerActions}>
                          <PostalCodeGetter
                            Button
                            shouldShowNumberKeyboard
                            submitLabel="Buscar"
                            onSubmit={submitPostalCode}
                          />
                        </div>
                      </div>
                    )}
                  </>
                ) : (
                  <CompleteAddress
                    address={address}
                    slas={data?.shipping?.logisticsInfo?.[0]?.slas}
                    pickups={dataPickup?.searchPickupPoint?.items}
                    resetAddress={handleResetAddress}
                  />
                )}
              </div>
            )}
          </AddressContainer>
        </AddressRules>
      </EXPERIMENTAL_Modal>
    </OrderShippingProvider>
  )
}

export default ShippingModal
