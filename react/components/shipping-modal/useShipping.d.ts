/* eslint-disable prefer-destructuring */
/* eslint-disable vtex/prefer-early-return */
import { useCallback, useEffect, useState } from 'react'
import { useMutation } from 'react-apollo'
import { useOrderForm } from 'vtex.order-manager/OrderForm'
import { useOrderShipping } from 'vtex.order-shipping/OrderShipping'
import { useRuntime } from 'vtex.render-runtime'

import UPDATE_ITEMS from './graphql/UPDATE_ITEMS.gql'
import UPDATE_REGION from './graphql/UPDATE_REGION.gql'
import UPDATE_SESSION_ADDRESS from './graphql/UPDATE_SESSION_ADDRESS.gql'

const useShipping = () => {
  const [fakeLoading, setFakeLoading] = useState(false)
  const { setOrderForm, orderForm } = useOrderForm()

  const [updateItems] = useMutation(UPDATE_ITEMS)
  const [
    updateRegion,
    { error: regionError, loading: regionLoading, data: regionData },
  ] = useMutation(UPDATE_REGION)

  const [
    updateAddressRegion,
    {
      error: regionAddressError,
      loading: regionAddressLoading,
      data: regionAddressData,
    },
  ] = useMutation(UPDATE_SESSION_ADDRESS)

  const {
    insertAddress,
    selectPickupOption,
    selectDeliveryOption,
    updateSelectedAddress,
  } = useOrderShipping()

  const {
    culture: { country },
  } = useRuntime()

  const { page } = useRuntime()

  useEffect(() => {
    if (regionError || regionAddressError) {
      console.error(regionError)
      console.error(regionAddressError)

      window.alert('Um erro aconteceu. Por favor, tente novamente mais tarde')

      setFakeLoading(false)
    }
  }, [regionError, regionAddressError])

  useEffect(() => {
    if (
      (!regionLoading && regionData?.updateRegion) ||
      (!regionAddressLoading && regionAddressData?.updateSessionAddress)
    ) {
      if (page === 'store.product') {
        localStorage.setItem('isPdp', 'true')
      }

      setTimeout(() => {
        window.location.reload()
      }, 2000)
    }
  }, [regionLoading, regionAddressLoading])

  const orderShippingCallback = useCallback(
    ({ success, orderForm: orderFormResponse }: any) => {
      if (success && orderFormResponse) {
        setOrderForm(orderFormResponse)
      }
    },
    [setOrderForm]
  )

  const submitPickupAddress = useCallback(
    async (selectedStore?: any, unavailableItems?: any) => {
      if (!selectedStore) {
        return
      }

      setFakeLoading(true)

      if (unavailableItems?.length) {
        await updateItems({
          variables: {
            orderItems:
              unavailableItems.map((item, index) => ({
                id: parseInt(item.id, 10),
                index,
                quantity:
                  item.simulationAvailability === 'withoutStock' ||
                  item.simulationAvailability === 'itemQuantityNotAvailable'
                    ? 0
                    : item.quantity,
                seller: item.seller,
                uniqueId: item.uniqueId,
              })) ?? [],
          },
        })
      }

      const update = {
        public: {
          country: {
            value: 'BRA',
          },
          postalCode: {
            value: selectedStore.pickupStoreInfo.address.postalCode,
          },
        },
      }

      const options = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(update),
      }

      const fetchSession = await fetch('/api/sessions', options)
      const { segmentToken } = await fetchSession.json()
      const { regionId } = JSON.parse(atob(segmentToken))
      console.info('regionIdregionId', regionId)

      // const regionStore = selectedStore.pickupPointId.split('_')[0]

      await insertAddress({
        ...selectedStore.pickupStoreInfo.address,
        addressType: 'search',
        addressId: '',
        receiverName: '',
      })
        .then((resp: any) => {
          console.info('insertAddress', resp)
          orderShippingCallback(resp)
        })
        .catch((err: any) => {
          console.error(err)
        })

      await updateSelectedAddress({
        ...selectedStore.pickupStoreInfo.address,
        addressType: 'search',
        addressId: '',
        receiverName: '',
      })
        .then((resp: any) => {
          console.info('updateSelectedAddress', resp)
          orderShippingCallback(resp)
        })
        .catch((err: any) => {
          console.error(err)
        })

      await updateAddressRegion({
        variables: {
          country,
          postalCode: selectedStore.pickupStoreInfo.address.postalCode,
        },
      })

      selectPickupOption(selectedStore.pickupPointId)
      localStorage.setItem('@mambo-pickup', selectedStore.friendlyName)
      localStorage.setItem('@mambo-store-id', selectedStore.pickupPointId)
      localStorage.setItem('@mambo-city', '')
    },
    [
      insertAddress,
      orderShippingCallback,
      updateRegion,
      selectPickupOption,
      orderForm,
    ]
  )

  const submitDeliveryAddress = useCallback(
    async (address?: any) => {
      if (!address) {
        return
      }

      setFakeLoading(true)

      updateAddressRegion({
        variables: {
          country,
          postalCode: address.postalCode,
        },
      }).then((resp) => {
        console.info('updateAddressRegion', resp)
      })

      await insertAddress(address).then((resp: any) => {
        console.info('insertAddress', address)
        orderShippingCallback(resp)
      })

      selectDeliveryOption(address.addressId)
      localStorage.setItem('@mambo-city', address.city)
      localStorage.setItem('@mambo-pickup', '')
    },
    [insertAddress, orderShippingCallback, selectDeliveryOption, setFakeLoading]
  )

  return {
    loading: fakeLoading,
    error: regionError ?? regionAddressError,
    submitPickupAddress,
    submitDeliveryAddress,
  }
}

export default useShipping
