/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable vtex/prefer-early-return */
import React, { useState, useEffect } from 'react'
import { removeValidation } from 'vtex.address-form/helpers'
import { Button } from 'vtex.styleguide'
import { useOrderForm } from 'vtex.order-manager/OrderForm'

import DeliveryData from './DeliveryData'
import PickupData from './PickupData'
import useShipping from './useShipping'
import styles from './styles.css'
import ChangeStoreInfo from './ChangeStoreInfo'

interface ICompleteAddressProps {
  address: AddressWithValidation
  slas: any[]
  pickups: any[]
  resetAddress: () => void
}

const CompleteAddress: React.FunctionComponent<ICompleteAddressProps> = ({
  address,
  slas,
  pickups,
  resetAddress,
}) => {
  const [orderDeliveryType, setOrderDeliveryType] = useState<
    'pickup' | 'delivery' | null
  >(null)

  const [isInfoModalOpen, setInfoModalOpen] = useState<boolean>(false)
  const [isFakeLoading, setFakeLoading] = useState<boolean>(false)
  const [ignoreItemStep, setIgnoreItemStep] = useState<boolean>(false)
  const [productsWithSimulation, setProductWithSimulation] = useState<any>(null)
  const [productsFiltered, setProductsFiltered] = useState<any>(null)

  const {
    orderForm: {
      items,
      shipping: { selectedAddress },
    },
    loading: orderFormLoading,
  } = useOrderForm()

  const { submitDeliveryAddress, loading: loadingDelivery } = useShipping()

  const { submitPickupAddress, loading: loadingPickup } = useShipping()

  const [finalPickup, setFinalPickup] = useState<any>(null)

  const { delivery: deliveryInfo, pickup: pickupInfo } = slas.reduce(
    ({ delivery, pickup }, current) => {
      if (current.deliveryChannel === 'pickup-in-point') {
        return { delivery, pickup: [...pickup, current] }
      }

      return { pickup, delivery: current }
    },
    { delivery: undefined, pickup: [] }
  )

  if (!slas?.length && !pickups) {
    return <span>Loading</span>
  }

  const verifyPickupInfo = {
    id: pickupInfo.length ? pickupInfo[0].id : pickups?.[0]?.pickupPoint.id,
    pickupPointId: pickupInfo.length
      ? pickupInfo[0].pickupPointId
      : pickups?.[0]?.pickupPoint.id,
    deliveryChannel: 'pickup-in-point',
    friendlyName: pickupInfo.length
      ? pickupInfo[0].friendlyName
      : pickups?.[0]?.pickupPoint.friendlyName,
    pickupStoreInfo: {
      address: pickupInfo.length
        ? pickupInfo[0].pickupStoreInfo.address
        : pickups?.[0]?.pickupPoint.address,
      friendlyName: pickupInfo.length
        ? pickupInfo[0].friendlyName
        : pickups?.[0]?.pickupPoint.friendlyName,
    },
    shippingEstimate: '4h',
  }

  const cartSimulation = async () => {
    if (items?.length === 0) {
      submitPickupAddress(finalPickup ?? verifyPickupInfo)
    }

    setFakeLoading(true)

    const myHeaders = new Headers()

    myHeaders.append('Accept', 'application/json')
    myHeaders.append('Content-Type', 'application/json')

    const requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: JSON.stringify({
        items: items.map((item: any) => {
          return {
            id: item.id,
            quantity: item.quantity,
            seller:
              finalPickup?.pickupPointId.split('_')[0] ??
              verifyPickupInfo.pickupPointId.split('_')[0],
          }
        }),
        country: 'BRA',
        postalCode: selectedAddress?.postalCode,
      }),
    }

    const getSimulation = await fetch(
      '/api/checkout/pub/orderforms/simulation?RnbBehavior=0',
      requestOptions
    )

    const products = await getSimulation.json()

    setFakeLoading(false)

    return setProductWithSimulation(products.items)
  }

  const handleSubmitAddress = () => {
    if (orderDeliveryType === 'delivery') {
      submitDeliveryAddress(removeValidation(address))
    }

    if (orderDeliveryType === 'pickup') {
      cartSimulation()
    }
  }

  useEffect(() => {
    if (!productsWithSimulation) {
      return
    }

    const unavailableItems = productsWithSimulation.filter(
      (item: any) =>
        item.availability === 'withoutStock' ||
        item.availability === 'itemQuantityNotAvailable'
    )

    if (unavailableItems.length > 0 && !ignoreItemStep) {
      items.map(
        (orderFormitem: any, index: number) =>
          (orderFormitem.simulationAvailability =
            productsWithSimulation[index].availability)
      )

      setProductsFiltered(items)

      return setInfoModalOpen(true)
    }

    submitPickupAddress(finalPickup ?? verifyPickupInfo, productsFiltered)
  }, [productsWithSimulation])

  return (
    <>
      <div className={styles.content}>
        <h3 className={`${styles.title} mb5 mt0 flex`}>
          No CEP, <span className="f7">{address.postalCode.value}</span> você
          pode:
        </h3>
        <div className={`${styles.deliveryText} mb5`}>
          <div
            className={`${styles.infoCardsContainer} flex flex-column flex-row-ns w-100`}
          >
            <PickupData
              pickupInfo={verifyPickupInfo}
              orderDeliveryType={orderDeliveryType}
              setOrderDeliveryType={setOrderDeliveryType}
              setFinalPickup={setFinalPickup}
            />
            <DeliveryData
              deliveryInfo={deliveryInfo}
              address={removeValidation(address)}
              orderDeliveryType={orderDeliveryType}
              setOrderDeliveryType={setOrderDeliveryType}
            />
          </div>
        </div>
        <p className={styles.minimumCartValue}>
          O valor mínimo de compra para qualquer modalidade de entrega é de R$
          70,00
        </p>
        <Button
          onClick={() => handleSubmitAddress()}
          disabled={!orderDeliveryType}
          isLoading={
            loadingDelivery ||
            loadingPickup ||
            orderFormLoading ||
            isFakeLoading
          }
        >
          Continuar
        </Button>
        <div className={`${styles.deliveryTextInfo} mt5 tr`}>
          <Button onClick={() => resetAddress()}>
            Alterar meu endereço de entrega
          </Button>
        </div>
      </div>
      <ChangeStoreInfo
        setInfoModalOpen={setInfoModalOpen}
        isInfoModalOpen={isInfoModalOpen}
        handleSubmitAddress={handleSubmitAddress}
        loadingPickup={loadingPickup}
        setIgnoreItemStep={setIgnoreItemStep}
        filteredItems={productsFiltered}
        selectedStore={
          finalPickup?.friendlyName ?? verifyPickupInfo.friendlyName
        }
      />
    </>
  )
}

export default CompleteAddress
