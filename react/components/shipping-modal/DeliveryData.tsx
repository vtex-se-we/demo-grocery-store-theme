import * as React from 'react'
import { Button } from 'vtex.styleguide'
import { useEffect } from 'react'
import { FormattedCurrency } from 'vtex.format-currency'
import { useOrderForm } from 'vtex.order-manager/OrderForm'

import styles from './styles.css'
import { IconDelivery, PinIcon } from '../../assets'

interface IDeliveryDataProps {
  deliveryInfo: any
  setOrderDeliveryType: (arg: 'pickup' | 'delivery' | null) => void
  orderDeliveryType: 'pickup' | 'delivery' | null
  address: any
}

const DeliveryData: React.FunctionComponent<IDeliveryDataProps> = ({
  deliveryInfo,
  setOrderDeliveryType,
  orderDeliveryType,
  address,
}) => {
  const { orderForm } = useOrderForm()
  const { selectedAddress } = orderForm.shipping

  useEffect(() => {
    if (!selectedAddress) {
      return
    }

    if (!selectedAddress.addressId.includes('gigadigital')) {
      setOrderDeliveryType('delivery')
    }
  }, [selectedAddress])

  if (!deliveryInfo) {
    return null
  }

  localStorage.setItem('valueFrete', deliveryInfo.price)

  return (
    <div
      key={deliveryInfo.id}
      className={`${styles.infoCards} ${
        orderDeliveryType === 'delivery' ? styles.deliveryTypeSelected : ''
      }`}
    >
      <Button
        onClick={() => {
          setOrderDeliveryType('delivery')
        }}
      >
        <div className={`${styles.infoCardsContent}`}>
          <div className={styles.infoCardsContentTitle}>
            <IconDelivery />
            <h3>Receber em casa</h3>
          </div>
          <div className={`${styles.infoCardsContentDelivery} mb4`}>
            <span>
              Frete a partir de{' '}
              <FormattedCurrency value={deliveryInfo.price / 100} />
            </span>
          </div>
          <div
            className={`${styles.deliveryAddress} tc flex items-center self-center`}
          >
            <PinIcon />
            <span className="pl2 f7 ttn pt2 b">{address.street}</span>
          </div>
        </div>
      </Button>
    </div>
  )
}

export default DeliveryData
