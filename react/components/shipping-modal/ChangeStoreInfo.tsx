/* eslint-disable react/jsx-pascal-case */
import React from 'react'
import { EXPERIMENTAL_Modal, Button } from 'vtex.styleguide'

import styles from './styles.css'

interface IMoreStoresModalProps {
  setInfoModalOpen: (param: boolean) => void
  isInfoModalOpen: boolean
  handleSubmitAddress: () => void
  loadingPickup: boolean
  setIgnoreItemStep: (param: boolean) => void
  filteredItems: any
  selectedStore: string
}

const ChangeStoreInfo: React.FunctionComponent<IMoreStoresModalProps> = ({
  setInfoModalOpen,
  isInfoModalOpen,
  handleSubmitAddress,
  loadingPickup,
  setIgnoreItemStep,
  filteredItems,
  selectedStore,
}) => {
  const handleCloseModal = () => {
    setInfoModalOpen(false)
  }

  return (
    // eslint-disable-next-line react/jsx-pascal-case
    <EXPERIMENTAL_Modal
      centered
      isOpen={isInfoModalOpen}
      showBottomBarBorder={false}
      showTopBar={false}
      closeOnOverlayClick
      closeOnEsc={false}
      showCloseIcon={false}
      onClose={() => handleCloseModal()}
      size="auto"
    >
      <div className={styles.containerChangeStoreInfo}>
        <div className="pb6">
          <div className={`${styles.titleChangeStoreInfo} pb4 b`}>
            Tem certeza que deseja alterar a loja selecionada?
          </div>
          <div className={styles.descriptionChangeStoreInfo}>
            Você está alterando para a loja {selectedStore}, e{' '}
            {filteredItems?.length > 1
              ? 'os seguintes produtos serão excluídos '
              : 'o seguinte produto será excluído '}{' '}
            do seu carrinho por{' '}
            {filteredItems?.length > 1
              ? 'estarem indisponíveis'
              : 'estar indisponível'}{' '}
            nessa loja:
          </div>
        </div>
        <div className={`${styles.unavailableListItem} mb6`}>
          {filteredItems?.map((item: any) =>
            item.simulationAvailability === 'withoutStock' ||
            item.simulationAvailability === 'itemQuantityNotAvailable' ? (
              <div className="flex items-center mb5">
                <div className="mr5 flex">
                  <img
                    width="52"
                    height="52"
                    src={item.imageUrls.at1x}
                    alt=""
                  />
                </div>
                <div className="mr5">{item.name}</div>
                <div className="ml-auto nowrap">{item.quantity} un.</div>
              </div>
            ) : (
              ''
            )
          )}
        </div>
        <div
          className={`${styles.buttonsChangeStoreInfo} flex flex-column flex-row-ns`}
        >
          <Button onClick={() => setInfoModalOpen(false)}>Cancelar</Button>
          <Button
            isLoading={loadingPickup}
            onClick={() => {
              setIgnoreItemStep(true)
              handleSubmitAddress()
            }}
          >
            Alterar Loja
          </Button>
        </div>
      </div>
    </EXPERIMENTAL_Modal>
  )
}

export default ChangeStoreInfo
