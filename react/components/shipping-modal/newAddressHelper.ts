let addressIdIncrement = 1

const getRandomAddressId = () =>
  (addressIdIncrement++ * new Date().getTime() * -1).toString().replace('-', '')

const newAddressHelper = (country: string) => {
  const addressId = getRandomAddressId()

  return {
    addressId,
    addressType: 'residential',
    city: null,
    complement: null,
    country,
    geoCoordinates: [],
    neighborhood: null,
    number: null,
    postalCode: null,
    receiverName: null,
    reference: null,
    state: null,
    street: null,
    addressQuery: null,
  }
}

export default newAddressHelper
