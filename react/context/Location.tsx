import React, { createContext, useEffect, useState } from 'react'

interface RegionContextData {
  region?: string
  pickup?: string
}

export const RegionContext = createContext<RegionContextData>(
  {} as RegionContextData
)

export const RegionProvider: React.FC = ({ children }) => {
  const [region, setRegion] = useState<string>()
  const [pickup, setPickup] = useState<string>()

  useEffect(() => {
    const deliveryCity = localStorage.getItem('@mambo-city')
    const pickupName = localStorage.getItem('@mambo-pickup')

    if (deliveryCity) {
      setRegion(deliveryCity)

      return
    }

    if (pickupName) {
      setPickup(pickupName.replace('Mambo Delivery ', 'Mambo '))

      return
    }

    setRegion('São Paulo')
  }, [])

  return (
    <RegionContext.Provider value={{ region, pickup }}>
      {children}
    </RegionContext.Provider>
  )
}
