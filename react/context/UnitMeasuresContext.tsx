import React, { createContext, useState, useEffect, useMemo } from 'react'
import { useProduct } from 'vtex.product-context'
import type { ProductTypes } from 'vtex.product-context'
import { useOrderForm } from 'vtex.order-manager/OrderForm'

import { mapCatalogItemToCart } from '../components/fixedPrices/modules/catalogItemToCart'

interface Product {
  productId: string
}

interface UnitMeasuresProvider {
  selectedQuantity: number
  unitMultiplierCustomNumber: number
  measurementUnitTextCustom: string | undefined
  mapUnitMeasureName: string[] | undefined
  validateQuantityOrderForm: string
  mapUnitMeasureQtyValueCardV2: string[] | undefined
  orderForm: {
    orderForm: {
      items: string[]
    }
  }
}

export const UnitContexts = createContext<UnitMeasuresProvider>(
  {} as UnitMeasuresProvider
)

export const UnitProvider: React.FC = ({ children }) => {
  const orderForm = useOrderForm()
  const productContextValue = useProduct()
  const product = productContextValue?.product
  const selectedItem = productContextValue?.selectedItem
  const assemblyOptions = productContextValue?.assemblyOptions
  const seller = productContextValue?.selectedItem
    ?.sellers[0] as ProductTypes.Seller
  const selectedQuantity =
    productContextValue?.selectedQuantity != null
      ? productContextValue.selectedQuantity
      : 1
  const [unitMultiplierCustomNumber, setUnitMultiplierCustomNumber] =
    useState(0)

  useEffect(() => {
    if (selectedItem) {
      setUnitMultiplierCustomNumber(
        Number(productContextValue?.selectedItem?.unitMultiplier)
      )
    }
  }, [selectedItem])

  const skuItems = useMemo(
    () =>
      mapCatalogItemToCart({
        product,
        selectedItem,
        selectedQuantity,
        selectedSeller: seller,
        assemblyOptions,
      }),
    [assemblyOptions, product, selectedItem, selectedQuantity, seller]
  )
  const measurementUnitTextCustom =
    productContextValue?.selectedItem?.measurementUnit

  const mapUnitMeasureName = productContextValue?.product?.properties.map(
    (item) => {
      if (item.name === 'FD') {
        return 'fardo'
      }
      if (item.name === 'CX') {
        return 'caixa'
      }
      if (item.name === 'KG') {
        return 'caixa'
      }
      return ''
    }
  )

  const validateQuantityOrderForm = orderForm?.orderForm?.items.filter(
    (item: Product) => item.productId === product?.productId
  )

  const mapUnitMeasureQtyValueCardV2 =
    productContextValue?.product?.properties?.[0]?.values

  useEffect(() => {
    if (productContextValue?.product?.properties[0]?.values[0]) {
      skuItems[0].quantity = Number(mapUnitMeasureQtyValueCardV2)
    }
  }, [
    validateQuantityOrderForm,
    skuItems,
    productContextValue,
    mapUnitMeasureQtyValueCardV2,
  ])

  return (
    <UnitContexts.Provider
      value={{
        selectedQuantity,
        unitMultiplierCustomNumber,
        measurementUnitTextCustom,
        mapUnitMeasureName,
        mapUnitMeasureQtyValueCardV2,
        orderForm,
        validateQuantityOrderForm,
      }}
    >
      {children}
    </UnitContexts.Provider>
  )
}
