import React from 'react'

import { RegionProvider } from './Location'
import ModalCEP from '../ModalCEP'

const ModalCEPContext: React.FC = () => (
  <RegionProvider>
    <ModalCEP />
  </RegionProvider>
)

export default ModalCEPContext
