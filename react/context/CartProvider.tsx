import React, { createContext, useContext, useState } from 'react'

interface ICartContext {
  selectedDepState: string
  setSelectedDepState: (arg: any) => void
}

const CartContext = createContext<ICartContext>({
  selectedDepState: '',
  setSelectedDepState: () => {
    // do nothing.
  },
})

const CartProvider = ({ children }) => {
  const [selectedDepState, setSelectedDepState] = useState('')
  return (
    <CartContext.Provider value={{ selectedDepState, setSelectedDepState }}>
      {children}
    </CartContext.Provider>
  )
}

export function useCartProvider() {
  const context = useContext(CartContext)
  if (!context) {
    throw new Error(
      'Esse contexto não pode ser usado fora do CartContextProvider'
    )
  }
  return context
}

export default CartProvider
