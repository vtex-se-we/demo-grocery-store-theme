import { defineMessages } from 'react-intl'

export const insertDelivery = defineMessages({
  title: { id: 'insert-delivery.title' },
  subtitle: { id: 'insert-delivery.subtitle' },
})

export const insertPickup = defineMessages({
  title: { id: 'insert-pickup.title' },
  subtitle: { id: 'insert-pickup.subtitle' },
})

export const insert = defineMessages({
  placeholder: { id: 'insert.placeholder' },
})

export const trigger = defineMessages({
  title: { id: 'trigger.title' },
  subtitle: { id: 'trigger.subtitle' },
  delivery: { id: 'trigger.delivery' },
  pickup: { id: 'trigger.pickup' },
  change: { id: 'trigger.change' },
})

export const pickupPoints = defineMessages({
  title: { id: 'pickup-points.title' },
  select: { id: 'pickup-points.select' },
  button: { id: 'pickup-points.button' },
})

export const modalHome = defineMessages({
  titleModal: { id: 'home.title' },
  subtitleModal: { id: 'home.subtitle' },
  deliveryCardTitle: { id: 'home-delivery.title' },
  deliveryCardSubtitle: { id: 'home-delivery.subtitle' },
  pickupCardTitle: { id: 'home-pickup.title' },
  pickupCardSubtitle: { id: 'home-pickup.subtitle' },
})

export const confirm = defineMessages({
  title: { id: 'confirm.title' },
  subtitle: { id: 'confirm.subtitle' },
  deliveryCardTitle: { id: 'confirm-delivery-card.title' },
  pickupCardTitle: { id: 'confirm-pickup-card.title' },
  cardEdit: { id: 'confirm-card.edit' },
  confirmButton: { id: 'confirm.button' },
})
