export function getFilterOrder(department) {
  switch (department) {
    case 'Bazar':
      return 10
    case 'Bebida':
      return 1
    case 'Carnes, Aves e Pescados':
      return 3
    case 'Congelados':
      return 4
    case 'Embalagens e Descartáveis':
      return 11
    case 'Frios e Laticínios':
      return 5
    case 'Higiene Pessoal e Beleza':
      return 8
    case 'Hortifruti':
      return 7
    case 'Limpeza':
      return 9
    case 'Mercearia':
      return 2
    case 'Padaria':
      return 6
    case 'Petshop':
      return 12
    case 'Migração (teste Mambo)':
      return 0
    default:
      return 0
  }
}
