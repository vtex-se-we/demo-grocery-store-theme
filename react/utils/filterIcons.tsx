import React from 'react'

import {
  Bazar,
  Bebida,
  CarnesAvesPescados,
  Congelados,
  Embalagens,
  FriosLaticineos,
  Higiene,
  Hortifruti,
  Limpeza,
  Mercearia,
  Padaria,
  Petshop,
  Todos,
  SelectedBazar,
  SelectedBebida,
  SelectedCarnesAvesPescados,
  SelectedCongelados,
  SelectedEmbalagens,
  SelectedFriosLaticineos,
  SelectedHigiene,
  SelectedHortifruti,
  SelectedLimpeza,
  SelectedMercearia,
  SelectedPadaria,
  SelectedPetshop,
  SelectedTodos,
} from '../assets'

export function FilterIcons(department, selectedDepState) {
  switch (department) {
    case 'Bazar':
      if (selectedDepState === 'Bazar') {
        return <SelectedBazar />
      }
      return <Bazar />

    case 'Bebida':
      if (selectedDepState === 'Bebida') {
        return <SelectedBebida />
      }
      return <Bebida />

    case 'Carnes, Aves e Pescados':
      if (selectedDepState === 'Carnes, Aves e Pescados') {
        return <SelectedCarnesAvesPescados />
      }
      return <CarnesAvesPescados />

    case 'Congelados':
      if (selectedDepState === 'Congelados') {
        return <SelectedCongelados />
      }
      return <Congelados />

    case 'Embalagens e Descartáveis':
      if (selectedDepState === 'Embalagens e Descartáveis') {
        return <SelectedEmbalagens />
      }
      return <Embalagens />

    case 'Frios e Laticínios':
      if (selectedDepState === 'Frios e Laticínios') {
        return <SelectedFriosLaticineos />
      }
      return <FriosLaticineos />

    case 'Higiene Pessoal e Beleza':
      if (selectedDepState === 'Higiene Pessoal e Beleza') {
        return <SelectedHigiene />
      }
      return <Higiene />

    case 'Hortifruti':
      if (selectedDepState === 'Hortifruti') {
        return <SelectedHortifruti />
      }
      return <Hortifruti />

    case 'Limpeza':
      if (selectedDepState === 'Limpeza') {
        return <SelectedLimpeza />
      }
      return <Limpeza />

    case 'Mercearia':
      if (selectedDepState === 'Mercearia') {
        return <SelectedMercearia />
      }
      return <Mercearia />

    case 'Padaria':
      if (selectedDepState === 'Padaria') {
        return <SelectedPadaria />
      }
      return <Padaria />

    case 'Petshop':
      if (selectedDepState === 'Petshop') {
        return <SelectedPetshop />
      }
      return <Petshop />

    case 'Migração (teste Mambo)':
      if (selectedDepState === 'Migração (teste Mambo)') {
        return <SelectedTodos />
      }
      return <Todos />

    default:
      if (!selectedDepState) {
        return <SelectedTodos />
      }
      return <Todos />
  }
}
