import { ValidationError } from 'yup'

interface ValidationErrorsProps {
  [key: string]: string
}

export default function getValidationErrors(
  err: ValidationError
): ValidationErrorsProps {
  const errors: ValidationErrorsProps = {}

  err.inner.forEach((e) => {
    if (e.path) {
      errors[e.path] = e.message
    }
  })

  return errors
}
