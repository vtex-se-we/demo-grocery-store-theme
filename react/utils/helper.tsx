export const correctDate = (date: string) => {
  if (date.includes('bd')) {
    return date.split('b')[0] === '1'
      ? '01 dia útil'
      : `${date.split('b')[0]} dias úteis`
  }

  return date.split('h')[0] === '1'
    ? '01 hora útil'
    : `${date.split('h')[0]} horas úteis`
}
